// Source :
// https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb

function hexToRgb(hex) {
  var res = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  res ? res={
    r: parseInt(res[1], 16),
    g: parseInt(res[2], 16),
    b: parseInt(res[3], 16)
  } : null;
  return vec4(res.r/255, res.g/255, res.b/255, 1.0);
}

function setColorHex(hex) {
  rgb = hexToRgb(hex);
  gl.uniform4f(colorUniformLocation, rgb[0], rgb[1], rgb[2], 1);
}

function setColorHexAlpha(hex, alpha) {
  rgb = hexToRgb(hex);
  gl.uniform4f(colorUniformLocation, rgb[0]*alpha, rgb[1]*alpha, rgb[2]*alpha, 1);
}

function setColorHexBg(hex) {
  rgb = hexToRgb(hex);
  gl.clearColor(rgb[0], rgb[1], rgb[2], 1);
}
