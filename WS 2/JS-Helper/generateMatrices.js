/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 2: Hierarchical Modelling & Lighting

Functions to generate matrices on hierarchichal model.
*/

/*
Normal, Ambient, and Diffuse Config Function

ambientColor: Ambient Color in Hex
diffuseColor: Diffuse Color in Hex

Configure normal matrix, ambient matrix, and diffuse matrix when drawing objects
*/
function configNAD(ambientColor, diffuseColor){
    // Normal Matrix
    var normalMatrix = [
        vec3(matrix[0][0], matrix[0][1], matrix[0][2]),
        vec3(matrix[1][0], matrix[1][1], matrix[1][2]),
        vec3(matrix[2][0], matrix[2][1], matrix[2][2])
    ];
    gl.uniformMatrix3fv(normalMatrixLoc, false, flatten(normalMatrix) );

    // Change Color from ambient
    var lightAmbient = createVec4(1.0);
    var materialAmbient = hexToRgb(ambientColor);
    ambientProduct = mult(lightAmbient, materialAmbient);
    gl.uniform4fv( ambientProductLoc, flatten(ambientProduct ));

    // Change Color from diffuse
    var lightDiffuse = createVec4(1.0);
    var materialDiffuse = hexToRgb(diffuseColor);
    diffuseProduct = mult(lightDiffuse, materialDiffuse);
    gl.uniform4fv( diffuseProductLoc, flatten(diffuseProduct) );

};

/*
Function to create the rotation matrix.
If animate is on use anim angle,
otherwise use the manual angle from slider.
*/
function setRotateMatrix(anim_angle, manual_angle, sumbu) {
    if (animate_on) {
        rotationMatrix = rotate(anim_angle, sumbu);
    } else {
        rotationMatrix = rotate(manual_angle, sumbu);
    }
}

/*
Function to create the rotation matrix
and save the current angle to the slider_angles array.

curAngle: is the angle to be rotated
bodyPartIndex: is the index number of the body part
               to be rotated
sumbu: the rotation axis
*/
function generateSliderAngles(curAngle, bodyPartIndex, sumbu){
    anim_angle = curAngle;
    manual_angle = slider_angles[bodyPartIndex];
    setRotateMatrix(anim_angle, manual_angle, sumbu);
    if (animate_on){
        slider_angles[bodyPartIndex] = anim_angle;
    }
    return rotationMatrix;
}