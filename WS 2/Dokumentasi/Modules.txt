### FILE DESCRIPTION

# Folder Common (Taken from WebGL-master)

	- gl-matrix.js 			: Library for vectors and matrices operation in WebGL.
	- glMatrix-0.9.5.min.js : Library for vectors and matrices operation in WebGL (minimized version).
	- initShaders.js 		: Initiate Shader in HTML.
	- initShaders2.js 		: Initiate shader in different files.
	- MV.js 				: our matrix/vector package. Documentation on website
	- MV2.js 				: our matrix/vector operation package. Documentation on website
	- webgl-utils.js 		: standard utilities from google to set up a webgl context


# Folder JS-Helper

	- arrayManipulation.js 	: Functions to help the process of creating vertices and arrays.
	- canvasResize.js 		: Adjust canvas size script.
	- hexToRgb.js 			: Convert hex color into RGB format.
	- generateMatrices.js	: Generate matrices that is commonly used for drawing objects.


- drawFunction.js 	: General functions to be called in render and functions to draw each component of figure.
- manAndRabbit.html : HTML for WebGL figure display.
- manAndRabbit.js 	: Main function and render function.
