Created and Developed By: 
-- Anna Ou --
Raihansyah Attallah Andrian - 1706040196
Rafif Taris - 1706979436
Gusti Ngurah Yama Adi Putra - 1706979253
Computer Graphics Course

Mid Term Exam - Task 2 - Teapot Observation

Sources Taken From:
- WebGL-master, Chap11, teapot4.js & teapot4.html
- WebGL-master, Chap 5, ortho.js

Program teapotObservation.js dan teapotObservation.html merupakan sebuah program observasi objek teapot yang memiliki 26 buah kamera di dalamnya. Gunakan arrow keys (up, down, left, right) untuk mengubah kamera. Dalam program juga terdapat satu button yang dapat membuat pergerakan teapot berhenti (toggle) yang ketika di klik berikutnya akan melanjutkan program sebelumnya.

Direktori Program:
- Pastikan teapotObservation.js dan teapotObservation.html berada pada satu direktori dengan patches.js serta vertices.js. 
- Pastikan folder "Nomor 2" sejajar dengan folder "Common".
- Buka teapotObservation.html untuk memulai program.

Pembagian Kerja No 2:
Raihansyah Attallah Andrian:
- Membuat 26 kamera pada program
- Menggabungkan program kamera dengan animasi dan toggle button

Rafif Taris:
- Membuat dokumentasi kode dan readme.txt
- Membersihkan kode (refactor)

Gusti Ngurah Yama Adi Putra:
- Membuat animasi pergerakan program
- Membuat toggle button