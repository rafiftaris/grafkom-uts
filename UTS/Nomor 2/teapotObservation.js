"use strict";
/*
Created and Developed By: 
Raihansyah Attallah Andrian - Rafif Taris - Gusti Ngurah Yama Adi Putra
1706040196 - 1706979436 - 1706979253
Computer Graphics Course

Mid Term Exam - Task 2

Sources Taken From:
- WebGL-master, Chap11, teapot4.js & teapot4.html
- WebGL-master, Chap 5, ortho.js
*/

//////////////// Template Vars /////////////////
var numDivisions = 3;

var index = 0;

var points = [];
var normals = [];

var modelViewMatrix = [];
var projectionMatrix = [];

var normalMatrix, normalMatrixLoc;

var axis =0;

var axis = 0;
var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var theta = [0, 0, 0];
var dTheta = 5.0;

var program;
var canvas, render, gl;

var bezier = function(u) {
    var b =new Array(4);
    var a = 1-u;
    b[3] = a*a*a;
    b[2] = 3*a*a*u;
    b[1] = 3*a*u*u;
    b[0] = u*u*u;
    return b;
}

var nbezier = function(u) {
    var b = [];
    b.push(3*u*u);
    b.push(3*u*(2-3*u));
    b.push(3*(1-4*u+3*u*u));
    b.push(-3*(1-u)*(1-u));
    return b;
}

////////////////// New Vars ////////////////////
/*

Camera Vars:
- thetaCamera: the theta for the camera
- radius, phi, dr as used in ortho.js

*/
var eye, at, up;
var radius = 1.0;
var thetaCamera = 0.0;
var phi = 0.0;
var dr = (5.0 * Math.PI) / 180.0;

/*

Moving Vectors:
- pos: increment vectors
- pos_to0: increment vectors when returning to origin
- ind: current movement index (0 - 5, at 6 returns to 0)
- axiss: the axis of direction (x = 0, y = 1, z = 2)
- to0_flag: true when returning to origin
- canMove: true when movement is allowed
- prevTheta: var to save the last theta

*/
var pos = [[0, 0 ,-0.02], [0, 0, 0.02],
           [-0.02, 0, 0], [0.02, 0, 0],
           [0, 0.02, 0], [0, -0.02, 0],];

var ind = 0;
var axiss = [2, 2, 0, 0, 1, 1]
var to0_flag = false;

var to0 = [[0, 0 , 0.02], [0, 0, -0.02],
           [0.02, 0, 0], [-0.02, 0, 0],
           [0, -0.02, 0], [0, 0.02, 0],];

var canMove = true;
var prevTheta;

onload = function init()  {

    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( 0.5, 0.5, 0.5, 1.0 );

    gl.enable(gl.DEPTH_TEST);

    /////////////// Init Camera location ////////////////
    at = vec3(0.0, 0.0, 0.0);
    up = vec3(0.0, 1.0, 0.0);

    initTeapot();

    ///////////////////// Init Shaders and Buffer //////////////////////
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );


    var nBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW );

    var vNormal = gl.getAttribLocation( program, "vNormal" );
    gl.vertexAttribPointer( vNormal, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vNormal);

    /////////////////////// Material and Light /////////////////////////
    projectionMatrix = ortho(-4, 4, -4, 4, -200, 200);
    gl.uniformMatrix4fv( gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));
    normalMatrixLoc = gl.getUniformLocation( program, "normalMatrix" );


    var lightPosition = vec4(0.0, 0.0, 20.0, 0.0 );
    var lightAmbient = vec4(0.2, 0.2, 0.2, 1.0 );
    var lightDiffuse = vec4( 1.0, 1.0, 1.0, 1.0 );
    var lightSpecular = vec4( 1.0, 1.0, 1.0, 1.0 );

    var materialAmbient = vec4( 1.0, 0.0, 1.0, 1.0 );
    var materialDiffuse = vec4( 1.0, 0.8, 0.0, 1.0 );
    var materialSpecular = vec4( 1.0, 0.8, 0.0, 1.0 );
    var materialShininess = 10.0;

    var ambientProduct = mult(lightAmbient, materialAmbient);
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);
    var specularProduct = mult(lightSpecular, materialSpecular);

    gl.uniform4fv( gl.getUniformLocation(program, "ambientProduct"),flatten(ambientProduct ));
    gl.uniform4fv( gl.getUniformLocation(program, "diffuseProduct"), flatten(diffuseProduct) );
    gl.uniform4fv( gl.getUniformLocation(program, "specularProduct"),flatten(specularProduct));
    gl.uniform4fv( gl.getUniformLocation(program, "lightPosition"), flatten(lightPosition ));
    gl.uniform1f( gl.getUniformLocation(program, "shininess"),materialShininess );

    // Key input function
    this.document.onkeyup = function(e){
        changeView(e);
    }

    // Toggle Button function
    document.getElementById("ButtonT").onclick = function(){
        if(canMove){
            canMove = false;
            prevTheta = theta;
        } else {
            canMove = true;
            theta = prevTheta;
        }
        // console.log(theta)
    };

    render();
}

var render = function(){
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //////////////// Transition Logic Flow ////////////////////
    if(canMove){
        if(to0_flag){
            var ind_to0 = ind - 1;

            if(ind_to0 == -1) ind_to0 = 5;

            for(var i=0; i<=2; i++) 
                theta[i] += to0[ind_to0][i];

            if(ind_to0 < 4){
                if(ind_to0%2 == 0 && theta[axiss[ind_to0]] > 0)
                    to0_flag = false;
                
                if(ind_to0%2 == 1 && theta[axiss[ind_to0]] < 0)
                    to0_flag = false;
            }
            else{
                if(ind_to0%2 == 0 && theta[axiss[ind_to0]] < 0)
                    to0_flag = false;
                
                if(ind_to0%2 == 1 && theta[axiss[ind_to0]] > 0)
                    to0_flag = false;
            }
        }
        else{
            for(var i=0; i<=2; i++) 
                theta[i] += pos[ind][i];

            if(ind < 4){
                if(ind%2 == 0 && theta[axiss[ind]] < -1){
                    to0_flag = true;
                    ind += 1;
                }
                
                if(ind%2 == 1 && theta[axiss[ind]] > 1){
                    to0_flag = true;
                    ind += 1;
                }
            }
            else{
                if(ind%2 == 0 && theta[axiss[ind]] > 1){
                    to0_flag = true;
                    ind += 1;
                }
                
                if(ind%2 == 1 && theta[axiss[ind]] < -1){
                    to0_flag = true;
                    ind += 1;
                } 
            }
        }
    }

    if(ind == 6) ind = 0;

    // Draw the teapot
    modelTeapot();
    
    gl.drawArrays( gl.TRIANGLES, 0, index);
    requestAnimFrame(render);
}

var modelTeapot = function(){
    eye = vec3(
        radius * Math.sin(phi),
        radius * Math.sin(thetaCamera),
        radius * Math.cos(phi)
    );

    modelViewMatrix = lookAt(eye, at, up);

    modelViewMatrix = mult(modelViewMatrix, translate(theta[0], theta[1], theta[2]));

    gl.uniformMatrix4fv( gl.getUniformLocation(program, "modelViewMatrix"), false, flatten(modelViewMatrix) );
    normalMatrix = [
        vec3(modelViewMatrix[0][0], modelViewMatrix[0][1], modelViewMatrix[0][2]),
        vec3(modelViewMatrix[1][0], modelViewMatrix[1][1], modelViewMatrix[1][2]),
        vec3(modelViewMatrix[2][0], modelViewMatrix[2][1], modelViewMatrix[2][2])
    ];
    gl.uniformMatrix3fv(normalMatrixLoc, false, flatten(normalMatrix) );
}

var initTeapot = function(){
    //////////////////////////// Init Teapot Start /////////////////////////////////
    var sum = [0, 0, 0];
    for(var i = 0; i<306; i++){
        for(j=0; j<3; j++)
            sum[j] += vertices[i][j];
    }
    for(j=0; j<3; j++) sum[j]/=306;
    for(var i = 0; i<306; i++){
        for(j=0; j<2; j++)
            vertices[i][j] -= sum[j]/2
    }
    for(var i = 0; i<306; i++){
        for(j=0; j<3; j++)
            vertices[i][j] *= 2;
    }


    var h = 1.0/numDivisions;

    var patch = new Array(numTeapotPatches);
    for(var i=0; i<numTeapotPatches; i++) patch[i] = new Array(16);

    for(var i=0; i<numTeapotPatches; i++){
        for(j=0; j<16; j++) {
            patch[i][j] = vec4([vertices[indices[i][j]][0],
                                vertices[indices[i][j]][2],
                                vertices[indices[i][j]][1], 1.0]);
        }
    }

    for ( var n = 0; n < numTeapotPatches; n++ ) {
        var data = new Array(numDivisions+1);
        for(var j = 0; j<= numDivisions; j++) data[j] = new Array(numDivisions+1);

        for(var i=0; i<=numDivisions; i++) for(var j=0; j<= numDivisions; j++) {
            data[i][j] = vec4(0,0,0,1);
            var u = i*h;
            var v = j*h;
            var t = new Array(4);
            for(var ii=0; ii<4; ii++) t[ii]=new Array(4);

            for(var ii=0; ii<4; ii++){
                for(var jj=0; jj<4; jj++){
                    t[ii][jj] = bezier(u)[ii]*bezier(v)[jj];
                }
            }


            for(var ii=0; ii<4; ii++) for(var jj=0; jj<4; jj++) {
                temp = vec4(patch[n][4*ii+jj]);
                temp = scale( t[ii][jj], temp);
                data[i][j] = add(data[i][j], temp);
            }
        }

        var ndata = new Array(numDivisions+1);
        for(var j = 0; j<= numDivisions; j++) ndata[j] = new Array(numDivisions+1);

        var tdata = new Array(numDivisions+1);
        for(var j = 0; j<= numDivisions; j++) tdata[j] = new Array(numDivisions+1);

        var sdata = new Array(numDivisions+1);
        for(var j = 0; j<= numDivisions; j++) sdata[j] = new Array(numDivisions+1);

        for(var i=0; i<=numDivisions; i++){
            for(var j=0; j<= numDivisions; j++) {
                ndata[i][j] = vec4(0,0,0,0);
                sdata[i][j] = vec4(0,0,0,0);
                tdata[i][j] = vec4(0,0,0,0);
                var u = i*h;
                var v = j*h;
                var tt = new Array(4);
                for(var ii=0; ii<4; ii++) tt[ii]=new Array(4);
                var ss = new Array(4);
                for(var ii=0; ii<4; ii++) ss[ii]=new Array(4);

                for(var ii=0; ii<4; ii++) for(var jj=0; jj<4; jj++) {
                    tt[ii][jj] = nbezier(u)[ii]*bezier(v)[jj];
                    ss[ii][jj] = bezier(u)[ii]*nbezier(v)[jj];
                }

                for(var ii=0; ii<4; ii++) 
                    for(var jj=0; jj<4; jj++) {
                        var temp = vec4(patch[n][4*ii+jj]); ;
                        temp = scale( tt[ii][jj], temp);
                        tdata[i][j] = add(tdata[i][j], temp);

                        var stemp = vec4(patch[n][4*ii+jj]); ;
                        stemp = scale( ss[ii][jj], stemp);
                        sdata[i][j] = add(sdata[i][j], stemp);

                    }
                temp = cross(tdata[i][j], sdata[i][j])

                ndata[i][j] =  normalize(vec4(temp[0], temp[1], temp[2], 0));
            }
        }

        for(var i=0; i<numDivisions; i++){
            for(var j =0; j<numDivisions; j++) {
                points.push(data[i][j]);
                normals.push(ndata[i][j]);

                points.push(data[i+1][j]);
                normals.push(ndata[i+1][j]);

                points.push(data[i+1][j+1]);
                normals.push(ndata[i+1][j+1]);

                points.push(data[i][j]);
                normals.push(ndata[i][j]);

                points.push(data[i+1][j+1]);
                normals.push(ndata[i+1][j+1]);

                points.push(data[i][j+1]);
                normals.push(ndata[i][j+1]);
                index+= 6;
            }
        }
    }
    //////////////////////////// Init Teapot End /////////////////////////////////
}

var changeView = function(e){
    /* 

    Function to change camera view

    - midTop and midBottom are the at vectors for camera
    when on middle top and middle bottom location
    - arrow consts marks the number of the key event
    - TOL for toleration counting error

    Note: TOL will result in counting error if rounding is done to many times.
    Increase TOL to make it more accurate

    */
    var midTop = vec3(0.0, -5, 0.0);
    var midBottom = vec3(0.0, 5, 0.0);
    const ARROW_LEFT = 37;
    const ARROW_UP = 38;
    const ARROW_RIGHT = 39;
    const ARROW_DOWN = 40;
    const TOL = 0.0001;
    
    // When on middle top
    if (midTop[1] == at[1]){
        at = vec3(0, 0, 0);
        // console.log("dari atas")
        if (e.which == ARROW_DOWN){
            at = vec3(0, 0, 0);
        } else if (e.which == ARROW_RIGHT){
            phi += 18 * dr;
        } else if (e.which == ARROW_LEFT){
            phi -= 18 * dr;
        } else if (e.which == ARROW_UP){
            phi += 36 * dr;
        }
    }
    // When on middle bottom
    else if (midBottom[1] == at[1]){
        at = vec3(0, 0, 0);
        // console.log("dari bawah")
        if (e.which == ARROW_UP){
            at = vec3(0, 0, 0);
        } else if (e.which == ARROW_RIGHT){
            phi += 18 * dr;
        } else if (e.which == ARROW_LEFT){
            phi -= 18 * dr;
        } else if (e.which == ARROW_DOWN){
            phi += 36 * dr;
        }
    }
    // Unallowed movements (corners)
    else if (e.which == ARROW_UP &&
            Math.abs(thetaCamera - 18 * dr) <= TOL &&
            thetaCamera != 0 &&
            Math.abs(((phi / dr) % 18) - 9) <= TOL){
        // console.log("gagal atas")
        return;
    }
    else if (e.which == ARROW_DOWN &&
            Math.abs(thetaCamera + 18 * dr) <= TOL &&
            thetaCamera != 0 &&
            Math.abs(((phi / dr) % 18) - 9) <= TOL){
        // console.log("gagal bawah")
        return;
    }
    // Move to middle top or bottom
    else if (e.which == ARROW_UP && Math.abs(thetaCamera - 18 * dr) <= TOL){
        // console.log("tengah atas")
        at = vec3(0.0, -5, 0.0);
    }
    else if (e.which == ARROW_DOWN && Math.abs(thetaCamera + 18 * dr) <= TOL){
        // console.log("tengah atas")
        at = vec3(0.0, 5, 0.0);
    }
    // Normal Cases
    else if (e.which == ARROW_UP) {
        // console.log("normal atas")
        thetaCamera += 18 * dr;
    }
    else if (e.which == ARROW_DOWN) {
        // console.log("normal bawah")
        thetaCamera -= 18 * dr;
    }
    else if (e.which == ARROW_RIGHT) {
        // console.log("normal kanan")
        phi += 9 * dr;
    }
    else if (e.which == ARROW_LEFT) {
        // console.log("normal kiri")
        phi -= 9 * dr;
    }

    // console.log(thetaCamera);
    // console.log(phi);
    // console.log(thetaCamera - 18 * dr);
    // console.log(18 * dr);
    // console.log((phi / dr) % 18);

}