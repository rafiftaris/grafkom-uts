Created and Developed By: 
-- Anna Ou --
Raihansyah Attallah Andrian - 1706040196
Rafif Taris - 1706979436
Gusti Ngurah Yama Adi Putra - 1706979253
Computer Graphics Course

Mid Term Exam - Task 1 - Simple Maze Game

Sources Taken From:
- https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Using_textures_in_WebGL (Init buffer and textures for objects)
- https://colorlib.com/wp/template/comodo/ (html template)
- https://rafift.imgur.com/all/

How To Play:
- Pastikan anda terhubung dengan koneksi internet. (Game ini menggunakan asset yang diupload ke internet)
- Objektif anda adalah menggerakkan Princess agar dapat bertemu dengan pangerannya (berwarna biru).
- Gunakan arrow keys (left, right, up, down) untuk menggerakkan Princess.
- Pada saat Princess digerakkan, ia akan selalu bergerak hingga tidak dapat bergerak lagi (ketika bertemu suatu wall).
- Namun, Princess dapat merubah arah gerakannya di tengah pergerakan bila terdapat space yang kosong (walaupun belum berhenti).
- Ketika Princess berhasil bertemu dengan Prince, maka anda telah berhasil menyelesaikan level tersebut!

- Untuk melihat demo, dari index.html klik "Game Demo"
- Terdapat empat level dalam game ini. Pilih level melalui index.html.
- Setelah satu level berhasil di menangkan, game akan otomatis ke level berikutnya.

Direktori Program:
- Pastikan file index.html, finish.html, maze.html, dan maze.js berada dalam satu direktori.
- Pastikan pula folder Assets, css, fonts, img, js, scss, dan vendors berada pada direktori yang sama.
- Pastikan folder "Nomor 1" sejajar dengan folder "Common".
- Buka index.html untuk memulai program.

Pembagian Kerja No 1:
Raihansyah Attallah Andrian:
- Membuat dokumentasi kode dan readme.txt
- Membersihkan kode (refactor)
- Level Design

Rafif Taris:
- Membuat movement karakter
- Membuat logic tembok pada labirin

Gusti Ngurah Yama Adi Putra:
- Membuat logic win pada labirin
- Membuat main menu dan UI game