"use strict";
/*
Created and Developed By: 
Raihansyah Attallah Andrian - Rafif Taris - Gusti Ngurah Yama Adi Putra
1706040196 - 1706979436 - 1706979253
Computer Graphics Course

Mid Term Exam - Task 1

Sources Taken From:

*/

// Maze in matrix form
// Second level maze
var level_1_maze;
var level_2_maze;
var level_3_maze;
var level_4_maze=[
    ['x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x'],//0
    ['x','p','p','p','p','p','p','p','p','p','x','x','p','p','p','p','p','p','p','x'],//1
    ['x','p','x','x','x','x','x','p','x','p','p','p','p','x','x','x','p','x','x','x'],//2
    ['x','p','p','p','p','p','p','x','p','x','x','p','x','p','x','p','p','p','p','x'],//3
    ['x','x','x','p','x','p','x','p','p','p','x','p','x','p','x','p','x','x','p','x'],//4
    ['x','p','x','p','x','p','x','p','x','p','x','p','p','p','p','x','p','p','p','x'],//5
    ['x','p','p','p','x','p','p','p','p','x','x','p','x','p','x','p','x','p','x','x'],//6
    ['x','p','x','p','x','p','x','p','x','p','p','x','p','p','p','p','p','x','p','x'],//7
    ['x','p','x','x','p','x','p','p','p','p','p','p','x','p','x','p','x','f','p','x'],//8
    ['x','p','p','p','p','p','x','x','p','x','x','p','p','x','p','p','x','x','p','x'],//9
    ['x','p','x','p','x','p','p','p','p','x','p','p','x','p','p','p','p','p','p','x'],//10
    ['x','p','x','p','x','p','x','x','x','p','x','p','x','p','x','p','x','x','x','x'],//11
    ['x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x'],//12
    ];
var current_maze = level_4_maze;

/* draw each objects */
function drawScene(){
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Set Projection Matrix and model view matrix
    mat4.perspective(45, canvas.width / canvas.height, 0.1, 100.0, projectionMatrix);
    mat4.identity(modelViewMatrix);
    mat4.translate(modelViewMatrix, [-18,12, -35.0]);
    mat4.rotate(modelViewMatrix, degToRad(90), [1, 0, 0]);

    // CREATING PLAYER
    pushMatrix();
    mat4.translate(modelViewMatrix, [xTrans,0.0 , yTrans]);
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexCoordBuffer);
    gl.vertexAttribPointer(program.vertexPositionAttribute, cubeVertexCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureBuffer);
    gl.vertexAttribPointer(program.textureCoordAttribute, cubeVertexTextureBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, PlayerTexture);
    gl.uniform1i(program.samplerUniform, 0);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexCoordBuffer);
    setMatrixUniforms();
    // Draw elements in triangle
    gl.drawElements(gl.TRIANGLES, cubeVertexIndicesBuffer.numVertices, gl.UNSIGNED_SHORT, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexCoordBuffer);
    popMatrix();
    gl.vertexAttribPointer(program.vertexPositionAttribute, cubeVertexCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    // CREATING MAZE
    for (var i=0; i<13; i++){
        for (var j=0;j<20;j++){
            if(current_maze[i][j]=="x"){
                pushMatrix();
                mat4.translate(modelViewMatrix,[(j*2),0,(i*2)]);
                gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexCoordBuffer);
                gl.vertexAttribPointer(program.vertexPositionAttribute,cubeVertexCoordBuffer.itemSize,gl.FLOAT,false,0,0);
                gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureBuffer);
                gl.vertexAttribPointer(program.textureCoordAttribute, cubeVertexTextureBuffer.itemSize, gl.FLOAT, false, 0, 0);

                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D,wallTexture);
                gl.uniform1i(program.samplerUniform,0);

                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndicesBuffer);
                setMatrixUniforms();
                gl.drawElements(gl.TRIANGLES, cubeVertexIndicesBuffer.numVertices, gl.UNSIGNED_SHORT, 0);
		        popMatrix();
            }
            else if(current_maze[i][j]=="f"){
                pushMatrix();
                mat4.translate(modelViewMatrix,[(j*2),0,(i*2)]);
                gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexCoordBuffer);
                gl.vertexAttribPointer(program.vertexPositionAttribute,cubeVertexCoordBuffer.itemSize,gl.FLOAT,false,0,0);
                gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureBuffer);
                gl.vertexAttribPointer(program.textureCoordAttribute, cubeVertexTextureBuffer.itemSize, gl.FLOAT, false, 0, 0);

                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D,FinishTexture);
                gl.uniform1i(program.samplerUniform,0);

                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndicesBuffer);
                setMatrixUniforms();
                gl.drawElements(gl.TRIANGLES, cubeVertexIndicesBuffer.numVertices, gl.UNSIGNED_SHORT, 0);
		        popMatrix();
            }
        }
    }
}

window.onload = function init() {
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
    
    //Enable z-buffer for depth sorting
    gl.enable(gl.DEPTH_TEST);

    //
    //  Load shaders and initialize attribute buffers
    //
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    
    this.attribLocations = {
        vertexPosition : gl.getAttribLocation(program, 'aVertexPosition'),
        textureCoord : gl.getAttribLocation(program, 'aTextureCoord')
    }
    this.uniformLocations = {
        projectionMatrix : gl.getUniformLocation(program, 'uProjectionMatrix'),
        modelViewMatrix : gl.getUniformLocation(program, 'uModelViewMatrix'),
        uSampler : gl.getUniformLocation(program, 'uSampler')
    }
    this.initBuffers();
    this.initTexture();

    gl.clearColor( 0.0, 0.0, 0.0, 1.0 );

    //define the keyboard handlers
    document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp;

    // Start the game
    tick();
}