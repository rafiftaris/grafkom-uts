"use strict";
/*
Created and Developed By: 
Raihansyah Attallah Andrian - Rafif Taris - Gusti Ngurah Yama Adi Putra
1706040196 - 1706979436 - 1706979253
Computer Graphics Course

Mid Term Exam - Task 1

Sources Taken From:

*/

// Canvas things
var canvas;

var gl;

var vertices;//contains indices for chains of vertices to draw triangles/other geometry

var program;
var attribLocations;
var uniformLocations;

// Model View and Projection Matrices
var modelViewMatrix = mat4.create();
var projectionMatrix = mat4.create();
var modelViewMatrixStack = [];

// Object Textures
var wallTexture;
var FinishTexture;
var PlayerTexture;

// Object Coord Buffers
var cubeVertexIndicesBuffer;
var cubeVertexCoordBuffer;
var cubeVertexTextureBuffer;
var cubeVertexColorBuffer;

var WallTextureColorBuffer;
var PlayerTextureCoordBuffer;
var stageBuffer;

// Flag for keys
var keyFlag = [];

// Flags for movement
var playerLeftFlag=false;
var playerUpFlag=false;
var playerRightFlag=false;
var playerDownFlag=false;

// Start game flag
var startGame;
var isWin = false;

// Coordinate Transitions
// this is the starting coordinates of player inside the maze(matrix)
var xTrans = 2.0;
var yTrans = 2.0;

// Time variables for animation and ticks
var recentTime = 0;
var count = 0.0;

// Maze in matrix form
//First level maze
var level_1_maze=[
    ['x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x'],//0
    ['x','p','p','p','p','x','p','p','p','p','p','p','p','x','p','p','p','p','x'],//1
    ['x','x','x','x','p','x','p','x','x','x','x','x','p','x','p','x','x','x','x'],//2
    ['x','p','p','p','p','p','p','p','p','x','p','p','p','p','p','p','p','p','x'],//3
    ['x','U','x','x','p','x','x','x','p','x','p','x','x','x','p','x','x','U','x'],//4
    ['x','p','p','x','p','p','p','p','p','f','p','p','p','p','p','x','p','p','x'],//5
    ['x','x','p','x','p','x','p','x','x','x','x','x','p','x','p','x','p','x','x'],//6
    ['x','p','p','p','p','x','p','p','p','x','p','p','p','x','p','p','p','p','x'],//7
    ['x','p','x','x','x','x','x','x','p','x','p','x','x','x','x','x','x','p','x'],//8
    ['x','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p','x'],//9
    ['x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x','x'],//10
    ];
var current_maze = level_1_maze;

/* Matrix operation for transformation */
//Push Matrix
function pushMatrix() {
    var copy = mat4.create();
    mat4.set(modelViewMatrix, copy);
    modelViewMatrixStack.push(copy);
}

//Pop Matrix
function popMatrix() {
    if (modelViewMatrixStack.length == 0) {
        throw "Invalid popMatrix!";
    }
    modelViewMatrix = modelViewMatrixStack.pop();
}

//Update matrix uniforms
function setMatrixUniforms() {
    gl.uniformMatrix4fv(program.projectionMatrixUniform, false, projectionMatrix);
    gl.uniformMatrix4fv(program.modelviewMatrixUniform, false, modelViewMatrix);
}

/* Handle general key input function */
function handleKeyDown(event){
    keyFlag[event.keyCode] = true;
}

function handleKeyUp(event){
    keyFlag[event.keyCode] = false;
}

//Rotation function helper
function degToRad(degrees) {
    return degrees * Math.PI / 180;
}

function isPowerOf2(value) {
    return (value & (value - 1)) == 0;
  }

/* Load texture and images to the cube */
function loadTextureConfig(texture,image) {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
    if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
        // Yes, it's a power of 2. Generate mips.
        gl.generateMipmap(gl.TEXTURE_2D);
     } else{
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
     }
}

/* Handle input and operate command based on inputs */
function handleKeyInput(){
    // console.log('xTrans: ' + xTrans);
    // console.log('yTrans: ' + yTrans);

    //37 for arrow left
    if (keyFlag[37]||playerLeftFlag) {

         //player idling or going right
        if(!playerUpFlag && !playerDownFlag && !playerLeftFlag){
            playerLeftFlag=true;
            playerRightFlag=false;
            playerUpFlag=false;
            playerDownFlag=false;
            xTrans -= 0.1;
        }

        // player wants to turn left after moving up 
        else if(playerUpFlag){
            // Predict if there is no block exist when turning left
            if(Math.ceil(yTrans+0.5)%2==1 && (yTrans+0.5)%2!=1 && current_maze[Math.floor(yTrans+0.5)/2][(xTrans-2)/2]!="x"){
                playerLeftFlag=true;
                playerRightFlag=false;
                playerUpFlag=false;
                playerDownFlag=false;
                xTrans -= 0.1;
                yTrans=Math.floor(yTrans+0.5); // adjusting the turn
            }
        }

        // player wants to turn left after moving down 
        else if(playerDownFlag){
            // Predict if there is no block exist when turning left
            if(Math.floor(yTrans+1.5)%2==1 && current_maze[Math.floor(yTrans+0.5)/2][(xTrans-2)/2]!="x"){
                playerLeftFlag=true;
                playerRightFlag=false;
                playerUpFlag=false;
                playerDownFlag=false;
                xTrans -= 0.1;
                yTrans=Math.floor(yTrans+0.5); // adjusting the turn
            }
        }
        
        // while going left, prevent going through the wall
        else if(playerLeftFlag){
            if((xTrans%2==0 && current_maze[yTrans/2][(xTrans-2)/2]!="x") || (xTrans%2!=0 && current_maze[yTrans/2][(Math.floor(xTrans)-1)/2]!="x")){
                playerLeftFlag=true;
                playerRightFlag=false;
                playerUpFlag=false;
                playerDownFlag=false;
                xTrans -= 0.1;
            }
        }
    }

    //39 for arrow right
    if (keyFlag[39]||playerRightFlag) {
        //player idling or going left
        if(!playerUpFlag && !playerDownFlag && !playerRightFlag){
            playerRightFlag=true;
            playerLeftFlag=false;
            playerUpFlag=false;
            playerDownFlag=false;
            xTrans += 0.1;
        }
        
        // player wants to turn right after moving up 
        else if(playerUpFlag){
            // Predict if there is no block exist when turning right
            if(Math.ceil(yTrans+0.5)%2==1 && (yTrans+0.5)%2!=1 && current_maze[Math.floor(yTrans+0.5)/2][(xTrans+2)/2]!="x"){
                playerRightFlag=true;
                playerLeftFlag=false;
                playerUpFlag=false;
                playerDownFlag=false;
                xTrans += 0.1;
                yTrans=Math.floor(yTrans+0.5); // adjusting the turn
            }
        }
        
        // player wants to turn right after moving down 
        else if(playerDownFlag){
            // Predict if there is no block exist when turning right
            if(Math.floor(yTrans+1.5)%2==1 && current_maze[Math.floor(yTrans+0.5)/2][(xTrans+2)/2]!="x"){
                playerRightFlag=true;
                playerLeftFlag=false;
                playerUpFlag=false;
                playerDownFlag=false;
                xTrans += 0.1;
                yTrans=Math.floor(yTrans+0.5); // adjusting the turn
            }
        }

        // while going right, prevent going through the wall
        else if(playerRightFlag){
            if ((xTrans%2==0 && current_maze[yTrans/2][(xTrans+2)/2]!="x") || (xTrans%2!=0 && current_maze[yTrans/2][(Math.floor(xTrans)+2)/2]!="x")){
                playerRightFlag=true;
                playerLeftFlag=false;
                playerUpFlag=false;
                playerDownFlag=false;
                xTrans += 0.1;
            }	
        }
    }
    
    //38 for arrow up
    if (keyFlag[38]||playerUpFlag) {
        //player idling or going down
        if(!playerLeftFlag && !playerRightFlag && !playerUpFlag){
            playerLeftFlag=false;
            playerUpFlag=true;
            playerRightFlag=false;
            playerDownFlag=false;
            yTrans -= 0.1;
        }
        
        // player wants to turn up after moving right 
        else if(playerRightFlag){
            // Predict if there is no block exist when turning up
            if(Math.floor(xTrans+1.5)%2==1 && current_maze[Math.floor((yTrans-2)/2)][Math.floor(xTrans+0.5)/2]!="x"){
                playerUpFlag=true;
                playerLeftFlag=false;
                playerRightFlag=false;
                playerDownFlag=false;
                xTrans=Math.floor(xTrans+0.5); // adjusting the turn
                yTrans -= 0.1;
            }
        }
         
        // player wants to turn up after moving left 
        else if(playerLeftFlag){
            // Predict if there is no block exist when turning left
            if(Math.ceil(xTrans+0.5)%2==1 && (current_maze[Math.floor((yTrans-2)/2)][Math.floor(xTrans+1.5)/2]!="x" && 
            current_maze[Math.floor((yTrans-2)/2)][Math.floor(xTrans+0.5)/2]!="x")){
                playerUpFlag=true;
                playerLeftFlag=false;
                playerRightFlag=false;
                playerDownFlag=false;
                xTrans=Math.floor(xTrans+0.5); // adjusting the turn
                yTrans -= 0.1;
            }
        } 
        
        // while going up, prevent going through the wall
        else if (playerUpFlag){
            if(current_maze[Math.ceil((yTrans-2)/2)][xTrans/2]!='x'){
                playerUpFlag=true;
                playerLeftFlag=false;
                playerRightFlag=false;
                playerDownFlag=false;
                yTrans -= 0.1;
            }
        }
    }

    //40 for arrow down
    if (keyFlag[40]||playerDownFlag) {
        //player idling or going down
        if(!playerLeftFlag && !playerRightFlag && !playerDownFlag){ 
            playerDownFlag=true;
            playerRightFlag=false;
            playerUpFlag=false;
            playerLeftFlag=false;
            yTrans += 0.1;
        }

        // player wants to turn up after moving right 
        else if(playerRightFlag){
            // Predict if there is no block exist when turning up
            if(Math.floor(xTrans+1.5)%2==1 && current_maze[(yTrans+2)/2][Math.floor(xTrans+0.5)/2]!="x"){
                playerDownFlag=true;
                playerLeftFlag=false;
                playerRightFlag=false;
                playerUpFlag=false;
                yTrans += 0.1;
                xTrans=Math.floor(xTrans+0.5); // adjusting the turn
            }
        }
        
        // player wants to turn up after moving left
        else if(playerLeftFlag){
            // Predict if there is no block exist when turning left
            if(Math.ceil(xTrans+0.5)%2==1 && (current_maze[(yTrans+2)/2][Math.floor(xTrans+1.5)/2]!="x" && 
            current_maze[(yTrans+2)/2][Math.floor(xTrans+0.5)/2]!="x")){
                playerDownFlag=true;
                playerLeftFlag=false;
                playerRightFlag=false;
                playerUpFlag=false;
                yTrans += 0.1;
                xTrans=Math.floor(xTrans+0.5); // adjusting the turn
            }
        }
                        
        // while going up, prevent going through the wall
        else if (playerDownFlag){
            if(current_maze[Math.floor((yTrans+2)/2)][xTrans/2]!='x'){
                playerUpFlag=false;
                playerLeftFlag=false;
                playerRightFlag=false;
                playerDownFlag=true;
                yTrans += 0.1;
            }
        }
    }
}

/* initialize buffer for object instances 
   source : https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Using_textures_in_WebGL
*/
function initBuffers(){
    // General Cube Buffer Object
    cubeVertexCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,cubeVertexCoordBuffer);

    var cube_sides_vertices = [
        // Front face
        -1.0, -1.0,  1.0,
         1.0, -1.0,  1.0,
         1.0,  1.0,  1.0,
        -1.0,  1.0,  1.0,

        // Back face
        -1.0, -1.0, -1.0,
        -1.0,  1.0, -1.0,
         1.0,  1.0, -1.0,
         1.0, -1.0, -1.0,

        // Top face
        -1.0,  1.0, -1.0,
        -1.0,  1.0,  1.0,
         1.0,  1.0,  1.0,
         1.0,  1.0, -1.0,

        // Bottom face
        -1.0, -1.0, -1.0,
         1.0, -1.0, -1.0,
         1.0, -1.0,  1.0,
        -1.0, -1.0,  1.0,

        // Right face
         1.0, -1.0, -1.0,
         1.0,  1.0, -1.0,
         1.0,  1.0,  1.0,
         1.0, -1.0,  1.0,

        // Left face
        -1.0, -1.0, -1.0,
        -1.0, -1.0,  1.0,
        -1.0,  1.0,  1.0,
        -1.0,  1.0, -1.0
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube_sides_vertices), gl.STATIC_DRAW);
    //3 coordinates x y z
    cubeVertexCoordBuffer.itemSize = 3;
    //24 vertices each cube
    cubeVertexCoordBuffer.numVertices = 24;

    // General cube texture buffer
    cubeVertexTextureBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,cubeVertexTextureBuffer);
    var textureCoordinates = [
        // Front
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Back
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Top
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Bottom
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Right
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Left
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
      ];
    
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates),gl.STATIC_DRAW);
    
    cubeVertexTextureBuffer.itemSize = 2;
    cubeVertexTextureBuffer.numVertices = 24;

    // Index Buffer to join sets of vertices into faces
    cubeVertexIndicesBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndicesBuffer);
    var cubeVertexIndices = [
		//this numbers are positions in the VBO array above
            0, 1, 2,      0, 2, 3,    // Front face
            4, 5, 6,      4, 6, 7,    // Back face
            8, 9, 10,     8, 10, 11,  // Top face
            12, 13, 14,   12, 14, 15, // Bottom face
            16, 17, 18,   16, 18, 19, // Right face
            20, 21, 22,   20, 22, 23  // Left face
        ];
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
    //one item - the cube itself
    cubeVertexIndicesBuffer.itemSize = 1;
    //36 indices per cube
    cubeVertexIndicesBuffer.numVertices = 36;

    PlayerTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,PlayerTextureCoordBuffer);
}

/* initialize textures for object instances 
   source : https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Using_textures_in_WebGL
*/
function initTexture() {
    // init texture
    wallTexture = gl.createTexture();
    FinishTexture = gl.createTexture();
    PlayerTexture = gl.createTexture();

    // set texture images
    wallTexture.image = new Image();
    FinishTexture.image = new Image();
    PlayerTexture.image = new Image();

    // Set Cross origin for each images
    wallTexture.image.crossOrigin = "anonymous";
    PlayerTexture.image.crossOrigin = "anonymous";
    FinishTexture.image.crossOrigin = "anonymous";
    
    // load texture configurations
    wallTexture.image.onload = function () {
        loadTextureConfig(wallTexture,wallTexture.image);}
    FinishTexture.image.onload = function () {
        loadTextureConfig(FinishTexture,FinishTexture.image);}
    PlayerTexture.image.onload = function () {
        loadTextureConfig(PlayerTexture,PlayerTexture.image);}

    // image source (must be url)
    wallTexture.image.src = "https://i.imgur.com/CbKw8b0.png";
    FinishTexture.image.src = "https://i.imgur.com/4IqWRnY.jpg";
    PlayerTexture.image.src = "https://i.imgur.com/FwXAC7O.jpg";
}

/* draw each objects */
function drawScene(){
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //Set Projection Matrix and model view matrix
    mat4.perspective(45, canvas.width / canvas.height, 0.1, 100.0, projectionMatrix);
    mat4.identity(modelViewMatrix);
    mat4.translate(modelViewMatrix, [-18,10, -35.0]);
    mat4.rotate(modelViewMatrix, degToRad(90), [1, 0, 0]);

    // CREATING PLAYER
    pushMatrix();
    mat4.translate(modelViewMatrix, [xTrans,0.0 , yTrans]);
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexCoordBuffer);
    gl.vertexAttribPointer(program.vertexPositionAttribute, cubeVertexCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureBuffer);
    gl.vertexAttribPointer(program.textureCoordAttribute, cubeVertexTextureBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, PlayerTexture);
    gl.uniform1i(program.samplerUniform, 0);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexCoordBuffer);
    setMatrixUniforms();
    // Draw elements in triangle
    gl.drawElements(gl.TRIANGLES, cubeVertexIndicesBuffer.numVertices, gl.UNSIGNED_SHORT, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexCoordBuffer);
    popMatrix();
    gl.vertexAttribPointer(program.vertexPositionAttribute, cubeVertexCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    // CREATING MAZE
    for (var i=0; i<11; i++){
        for (var j=0;j<19;j++){
            if(current_maze[i][j]=="x"){
                pushMatrix();
                mat4.translate(modelViewMatrix,[(j*2),0,(i*2)]);
                gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexCoordBuffer);
                gl.vertexAttribPointer(program.vertexPositionAttribute,cubeVertexCoordBuffer.itemSize,gl.FLOAT,false,0,0);
                gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureBuffer);
                gl.vertexAttribPointer(program.textureCoordAttribute, cubeVertexTextureBuffer.itemSize, gl.FLOAT, false, 0, 0);

                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D,wallTexture);
                gl.uniform1i(program.samplerUniform,0);

                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndicesBuffer);
                setMatrixUniforms();
                gl.drawElements(gl.TRIANGLES, cubeVertexIndicesBuffer.numVertices, gl.UNSIGNED_SHORT, 0);
		        popMatrix();
            }
            else if(current_maze[i][j]=="f"){
                pushMatrix();
                mat4.translate(modelViewMatrix,[(j*2),0,(i*2)]);
                gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexCoordBuffer);
                gl.vertexAttribPointer(program.vertexPositionAttribute,cubeVertexCoordBuffer.itemSize,gl.FLOAT,false,0,0);
                gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureBuffer);
                gl.vertexAttribPointer(program.textureCoordAttribute, cubeVertexTextureBuffer.itemSize, gl.FLOAT, false, 0, 0);

                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D,FinishTexture);
                gl.uniform1i(program.samplerUniform,0);

                gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndicesBuffer);
                setMatrixUniforms();
                gl.drawElements(gl.TRIANGLES, cubeVertexIndicesBuffer.numVertices, gl.UNSIGNED_SHORT, 0);
		        popMatrix();
            }
        }
    }
}

function checkWinCondition(){
    if (playerUpFlag){
        console.log(current_maze[Math.ceil((yTrans-2)/2)][xTrans/2]);
		if(current_maze[Math.ceil((yTrans-2)/2)][xTrans/2]=='f'){
            isWin = true;
        }
    }	
    else if (playerRightFlag){
	    if(current_maze[Math.floor(yTrans)/2][Math.floor((xTrans+0.5)/2)]=="f"){
            isWin = true;
        }
    }
    else if (playerLeftFlag){
        if((xTrans%2==0 && current_maze[yTrans/2][(xTrans-2)/2]=="f") || (xTrans%2!=0 && current_maze[yTrans/2][(Math.floor(xTrans)-1)/2]=="f")){
            isWin = true;
        }
    }
    else if (playerDownFlag){
        console.log(current_maze[Math.floor((yTrans+2)/2)][xTrans/2]);
        if(current_maze[Math.floor((yTrans+2)/2)][xTrans/2]=='f'){
            isWin = true;
        }
    }
    if (isWin){
        if(current_maze == level_1_maze){
            location.replace('maze2.html');
        } else if(current_maze == level_2_maze){
            location.replace('maze3.html');
        } else if(current_maze == level_3_maze){
            location.replace('maze4.html');
        } else if(current_maze == level_4_maze){
            location.replace('finish.html');
        }
    }

}

// For every tick :
// 1. Request new animation frame
// 2. Handle inputs for player movement
// 3. Check if win condition fulfilled
function tick(){
    window.requestAnimationFrame(tick);
    handleKeyInput();
    drawScene();
    checkWinCondition();
}

window.onload = function init() {
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
    
    //Enable z-buffer for depth sorting
    gl.enable(gl.DEPTH_TEST);

    //
    //  Load shaders and initialize attribute buffers
    //
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    
    this.attribLocations = {
        vertexPosition : gl.getAttribLocation(program, 'aVertexPosition'),
        textureCoord : gl.getAttribLocation(program, 'aTextureCoord')
    }
    this.uniformLocations = {
        projectionMatrix : gl.getUniformLocation(program, 'uProjectionMatrix'),
        modelViewMatrix : gl.getUniformLocation(program, 'uModelViewMatrix'),
        uSampler : gl.getUniformLocation(program, 'uSampler')
    }
    this.initBuffers();
    this.initTexture();

    gl.clearColor( 0.0, 0.0, 0.0, 1.0 );

    //define the keyboard handlers
    document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp;

    // Start the game
    tick();
}