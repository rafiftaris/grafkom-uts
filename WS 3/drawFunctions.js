/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 2: Hierarchical Modelling & Lighting
Worksheet 3: Texturing & Shadowing

The draw functions for each hierarchical model.
*/

// Variables for drawing
var offset = 0;
var prev_matrix;
var lookAtMatrix = lookAt([50,20,200], [0,0,0], [0,1,0]); // Look at position
var moveOriginMatrix; //move origin to 'center' of the part as center of rotation

// Rotation Matrices for limbs
var lowerArmRotationMatrix, upperArmRotationMatrix;
var legRotationMatrix;
var lowerRabbitLegRotationMatrix;
var headRotationMatrix;

// Default Width for Main Body
var lebar_connected = 10;
var lebar_body = 4*lebar_connected;

var anim_angle;
var manual_angle;

/* ============== HUMAN ============== */
// Human Figure Component Size
var torsoHeight = 50;
var torsoWidth = 30;
var armHeight = 20;
var armWidth  = 5;
var upperLegWidth  = 10;
var upperLegHeight = 50;
var headHeight = 20;
var headWidth = 15;

// Limb to define body parts
var limb = []
var lowerLimb = []

// Human Base Matrices
var base_body = translate(-125,-lebar_body/6,0);
var head_connected = translate(0-z_fight_num,torsoWidth/2,0);
var leg_connected = translate(0-z_fight_num,torsoHeight/1.3,0);
var arm_connected = translate(0-z_fight_num,torsoHeight*1.5,0);
var base_connected_new;
var base_connected_akhir;

// Human Limbs translation
var leg_blocks =[
    translate(torsoWidth/4, -torsoHeight*1.2, 0),
    translate(-torsoWidth/4, -torsoHeight*1.2, 0)
];
var arm_blocks = [
    translate((torsoWidth/1.7), -torsoHeight*1.2, 0),
    translate(-(torsoWidth/1.7), -torsoHeight*1.2, 0)
];

/* ============== RABBIT ============== */
// Rabbit Component Size
var rabbitTorsoHeight = 30;
var rabbitTorsoWidth = 60;
var rabbitTailHeight = 10;
var rabbitTailWidth  = 10;
var rabbitUpperLegWidth  = 20;
var rabbitUpperLegHeight = 20;
var rabbitLowerLegWidth  = 30;
var rabbitLowerLegHeight = 10;
var rabbitEarHeight = 25;
var rabbitEarWidth = 10;
var rabbitArmWidth  = 10;
var rabbitArmHeight = 40;
var rabbitHeadHeight = 30;
var rabbitHeadWidth = 30;

// Rabbit Base Matrices
var base_rabbit = translate(0,-lebar_body/6,0);
var rabbit_head_connected = translate(0-z_fight_num,rabbitTorsoWidth/2,0);
var rabbit_leg_connected = translate(0-z_fight_num,rabbitTorsoWidth/2.5,0);
var rabbit_tail_connected = translate(0-z_fight_num,rabbitTorsoHeight*1.5,0);
var rabbit_arm_connected = translate(0-z_fight_num,rabbitTorsoHeight,0);
var rabbit_ear_connected = translate(0-z_fight_num,rabbitHeadHeight/1.1,0);
var rabbit_lower_leg_connected = translate(0-z_fight_num,rabbitUpperLegHeight,0);
var rabbit_base_connected_new;
var rabbit_base_connected_akhir;

// Rabbit Limbs translation
var rabbit_leg_blocks =[
    translate(rabbitTorsoWidth/3, -rabbitTorsoHeight*1.5, rabbitTorsoWidth/3),
    translate(rabbitTorsoWidth/3, -rabbitTorsoHeight*1.5, -rabbitTorsoWidth/3),
]
var rabbit_lower_leg_blocks =[
    translate(-rabbitUpperLegWidth/2, -rabbitUpperLegHeight*2, rabbitUpperLegWidth/17),
    translate(-rabbitUpperLegWidth/2, -rabbitUpperLegHeight*2, -rabbitUpperLegHeight/17),
]
var rabbit_arm_blocks =[
    translate(-rabbitTorsoWidth/1.7, -rabbitTorsoHeight*1.2, rabbitTorsoWidth/3),
    translate(-rabbitTorsoWidth/1.7, -rabbitTorsoHeight*1.2, -rabbitTorsoWidth/3),
]
var rabbit_ear_blocks =[
    translate(rabbitHeadWidth/7, -rabbitHeadHeight, rabbitHeadWidth/4),
    translate(rabbitHeadWidth/7, -rabbitHeadHeight, -rabbitHeadWidth/4),
]

/*
Draw Light Source Function.
Called from render().

Size: Len of light square image
Basematrix: mat4()

In Result HTML shown in yellow dot (top-left)
*/
function drawLight(size, basematrix) {
    var block_arr = drawBlock(size, size, size);
    prev_matrix = basematrix;

    translation = block_arr[2];

    // Points to buffer
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(block_arr[0]), gl.STATIC_DRAW);

    // Converting matrixes
    matrix = mat4();
    matrix = mult(translate(lightPosition[0], lightPosition[1], lightPosition[2]), matrix)
    matrix = mult(prev_matrix, matrix);
    prev_matrix = matrix;
    var matrix_final = mult(lookAtMatrix, matrix);

    gl.uniformMatrix4fv( matrixLocation, false, flatten(matrix_final) );

    if(lights_on){
        configNAD("#EAF481", "#3c8ac2");
    } else{
        configNAD("#5d6134", "#153145");
    }

    // Drawing all
    gl.drawArrays( primitiveType, offset, block_arr[1] );
    return prev_matrix;
}

/*
The main function to draw man.
Called from render().
*/
function drawMan(){
    // Create Base
    var mat_body = drawBody(torsoHeight, torsoWidth, 20, base_body);

    // Create Connected Components
    // Head
    base_connected_new = mult(translate(0, torsoHeight/5, 0), head_connected);
    base_connected_akhir = mult(mat_body, base_connected_new);
    drawConnected(['head'], headHeight, headWidth, 10, 1, base_connected_akhir, 1.5);

    // Legs
    for (var i = 0; i < leg_blocks.length; i++){
        if(i==0){
                limb = ['legs','left']
        } else {
                limb = ['legs','right']
        }
        base_connected_new = mult(leg_blocks[i], leg_connected);
        base_connected_akhir = mult(mat_body, base_connected_new);
        drawConnected(limb, upperLegHeight, upperLegWidth, 10, 1, base_connected_akhir, 1);
    }

    // Arms
    for (var i = 0; i < arm_blocks.length; i++){
        if(i==0){
                limb = ['arms','right']
        } else {
                limb = ['arms','left']
        }
        base_connected_new = mult(arm_blocks[i], arm_connected);
        base_connected_akhir = mult(mat_body, base_connected_new);
        drawConnected(limb, armHeight, armWidth, 10, 2, base_connected_akhir, 1);
    }
}

/*
The main function to draw rabbit.
Called from render().
*/
function drawRabbit(){
    var rabbit_body = drawRabbitBody(rabbitTorsoHeight, rabbitTorsoWidth, 40, base_rabbit);

    // Rabbit head
    rabbit_base_connected_new = mult(translate(-rabbitTorsoWidth/2, -rabbitTorsoHeight/1.8, 0), rabbit_head_connected);
    rabbit_base_connected_akhir = mult(rabbit_body, rabbit_base_connected_new);
    var rabbit_head = drawConnected(['rabbitHead'], rabbitHeadHeight, rabbitHeadWidth, 30, 1, rabbit_base_connected_akhir, 0.5);

    // Rabbit Legs
    for (var i = 0; i < rabbit_leg_blocks.length; i++){
        if(i==0){
            limb = ['upperRabbitLeg','left']
            lowerLimb = ['lowerRabbitLeg','left']
        } else {
            limb = ['upperRabbitLeg','right']
            lowerLimb = ['lowerRabbitLeg','right']
        }
        rabbit_base_connected_new = mult(rabbit_leg_blocks[i], rabbit_leg_connected);
        rabbit_base_connected_akhir = mult(rabbit_body, rabbit_base_connected_new);
        var x = drawConnected(limb, rabbitUpperLegHeight, rabbitUpperLegWidth, 10, 1, rabbit_base_connected_akhir, 1.5);
        rabbit_base_connected_new = mult(rabbit_lower_leg_blocks[i], rabbit_lower_leg_connected);
        rabbit_base_connected_akhir = mult(x, rabbit_base_connected_new);
        drawConnected(lowerLimb, rabbitLowerLegHeight, rabbitLowerLegWidth, 10, 1, rabbit_base_connected_akhir, 0.5);
    }

    // Rabbit Tail
    rabbit_base_connected_new = mult(translate(rabbitTorsoWidth/1.8, -rabbitTorsoHeight*1.8, 0), rabbit_tail_connected);
    rabbit_base_connected_akhir = mult(rabbit_body, rabbit_base_connected_new);
    drawConnected(['rabbitTail'], rabbitTailHeight, rabbitTailWidth, 10, 1, rabbit_base_connected_akhir, 1.5)

    // Rabbit Arms
    for (var i = 0; i < rabbit_arm_blocks.length; i++){
        if(i==0){
            limb = ['rabbitArm','left']
        } else {
            limb = ['rabbitArm','right']
        }
        rabbit_base_connected_new = mult(rabbit_arm_blocks[i], rabbit_arm_connected);
        rabbit_base_connected_akhir = mult(rabbit_body, rabbit_base_connected_new);
        drawConnected(limb, rabbitArmHeight, rabbitArmWidth, 10, 1, rabbit_base_connected_akhir, 0.8);
    }

    // Rabbit Ears - depend to rabbit_head
    for (var i = 0; i < rabbit_ear_blocks.length; i++){
        if(i==0){
            limb = ['rabbitEar','right']
        } else {
            limb = ['rabbitEar','left']
        }
        rabbit_base_connected_new = mult(rabbit_ear_blocks[i], rabbit_ear_connected);
        rabbit_base_connected_akhir = mult(rabbit_head, rabbit_base_connected_new);
        drawConnected(limb, rabbitEarHeight, rabbitEarWidth, 10, 1, rabbit_base_connected_akhir, 0.75);
    }
}


/*
Draw Skybox
*/
function drawSkyBox(){
    var block_arr = drawSquareBlock(gl.canvas.height, gl.canvas.width);

    // Points to buffer
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(block_arr[0]), gl.STATIC_DRAW);

    // Drawing all
    gl.drawArrays( primitiveType, offset, block_arr[1] );
}

/*
Draw Body for man Function

Height: Height of Body
Width: Width of Body
Thick: Thickness of Body
Basematrix: base_body
*/
function drawBody(height, width, thick, basematrix) {
    var block_arr = drawBlock(height, width, thick);
    prev_matrix = basematrix;

    translation = block_arr[2];

    // Points to buffer
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(block_arr[0]), gl.STATIC_DRAW);

    // Converting matrixes
    matrix = mat4();

    // Set Angle Slider - Human Body
    generateSliderAngles(angle*2-30, idx_human_body, [0.2,1,0]);
    matrix = mult(rotationMatrix, matrix);

    matrix = mult(prev_matrix, matrix);
    prev_matrix = matrix;
    var matrix_final = mult(lookAtMatrix, matrix);

    gl.uniformMatrix4fv( matrixLocation, false, flatten(matrix_final) );

    configColors(idx_human_body);

    // Drawing all
    gl.drawArrays( primitiveType, offset, block_arr[1] );
    return prev_matrix;
}
  
/*
Draw Connected Components Function.
Connected components can be moved in the determined direction.

bodyparts: define bodypart of object (arr of string)
Height: Height of Connected
Width: Width of Connected
Thick: Thickness of Connected
Num: Number of stacking blocks
Basematrix: Connected Base matrix
Speed: Rotation Speed
ambientColor: Hex of Ambient Color
diffuseColor: Hex of Diffuse
*/
function drawConnected(bodyparts, height, width, thick, num, basematrix, speed) {
    var block_arr = drawBlock(height, width, thick);
    prev_matrix = basematrix;
    var color_index

    for (var i = 0; i < num; i++) {

        translation = block_arr[2];
        var translation_stacking = [0, height, 0];

        // Points to buffer
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(block_arr[0]), gl.STATIC_DRAW);

        // Converting matrices
        translationMatrix = translate(translation_stacking[0], translation_stacking[1], translation_stacking[2]);
        var translationZMatrix = translate(-i*z_fight_num,0,0);

        // Set rotation matrices
        rotationMatrix = rotate(0, [0,0,1]);
        
        /* HUMAN PARTS ROTATION MATRICES */
        if(bodyparts[0]=='arms'){
            if(i==0){ // Upper Arm
                if(bodyparts[1]=='right'){
                    // Set Angle Slider - Right Upper Arm
                    generateSliderAngles(150+angle*speed, idx_human_rua, [1,0,0]);
                    color_index = idx_human_rua;
                } else {
                    // Set Angle Slider - Left Upper Arm
                    generateSliderAngles(210-angle*speed, idx_human_lua, [1,0,0]);
                    color_index = idx_human_lua;
                }
            } else { // Lower Arm
                if(bodyparts[1]=='right'){
                    // Set Angle Slider - Right Lower Arm
                    generateSliderAngles(-110+angle*speed, idx_human_rla, [1,0,0]);
                    color_index = idx_human_rla;
                } else {
                    // Set Angle Slider - Left Lower Arm
                    generateSliderAngles(-50-angle*speed, idx_human_lla, [1,0,0]);
                    color_index = idx_human_lla;
                }
            }
        }

        if(bodyparts[0]=='legs'){
            if(bodyparts[1]=='left'){
                // Set Angle Slider - Left Leg
                generateSliderAngles(210-angle*speed, idx_human_ll, [1,0,0]);
                color_index = idx_human_ll;
            } else {    
                // Set Angle Slider - Right Leg
                generateSliderAngles(150+angle*speed, idx_human_rl, [1,0,0]);
                color_index = idx_human_rl;
            }
        }

        if(bodyparts[0]=='head'){
            // Set Angle Slider - Right Left Head
            var tempRotMatrix = generateSliderAngles(angle*speed-30, idx_human_rlhead, [0,1,0]);

            // Set Angle Slider - Up Down Head
            generateSliderAngles((angle*speed/2)-30, idx_human_udhead, [1,0,0]);
            color_index = idx_human_udhead;

            // Multiply both heads matrices
            rotationMatrix = mult(rotationMatrix, tempRotMatrix);
        }
        
        /* RABBIT PARTS ROTATION MATRICES */
        if(bodyparts[0]=='lowerRabbitLeg'){
            // Lower Legs
            if(bodyparts[1]=='right') {
                // Set Angle Slider - Right Lower Leg
                generateSliderAngles(200+angle*speed, idx_rabbit_rll, [0,0,1]);
                color_index = idx_rabbit_rll;
            } else {
                // Set Angle Slider - Left Lower Leg
                generateSliderAngles(200+angle*speed, idx_rabbit_lll, [0,0,1]);
                color_index = idx_rabbit_lll;
            }
        }

        if(bodyparts[0]=='upperRabbitLeg'){
            if(bodyparts[1]=='right') {
                // Set Angle Slider - Right Upper Leg
                generateSliderAngles(angle*speed, idx_rabbit_rul, [0,0,1]);
                color_index = idx_rabbit_rul;
            } else {
                // Set Angle Slider - Left Upper Leg
                generateSliderAngles(angle*speed, idx_rabbit_lul, [0,0,1]);
                color_index = idx_rabbit_lul;
            }
        }

        if(bodyparts[0]=='rabbitHead'){
            // Set Angle Slider - Head Up Down
            generateSliderAngles(angle*speed, idx_rabbit_head, [0,0,1]);
            color_index = idx_rabbit_head;
        }

        if(bodyparts[0]=='rabbitArm'){
            if(bodyparts[1]=='right') {
                // Set Angle Slider - Right Hand
                generateSliderAngles(180+angle*speed, idx_rabbit_rhand, [0,0,1]);
                color_index = idx_rabbit_rhand;
            } else {
                // Set Angle Slider - Left Hand
                generateSliderAngles(180+angle*speed, idx_rabbit_lhand, [0,0,1]);
                color_index = idx_rabbit_lhand;
            }
        }

        if(bodyparts[0]=='rabbitEar'){
            if(bodyparts[1]=='left') {
                // Set Angle Slider - Left Ear
                generateSliderAngles(angle*speed, idx_rabbit_rear, [0,0,1]);
                color_index = idx_rabbit_rear;
            } else {
                // Set Angle Slider - Right Ear
                generateSliderAngles(angle*speed, idx_rabbit_lear, [0,0,1]);
                color_index = idx_rabbit_lear;
            }
        }

        if(bodyparts[0]=='rabbitTail'){
            color_index = idx_rabbit_tail;
        }

        moveOriginMatrix = translate(translation[0], translation[1], translation[2]);

        // Multiply matrices
        matrix = mat4();
        matrix = mult(moveOriginMatrix, matrix);
        matrix = mult(translationZMatrix, matrix);
        matrix = mult(rotationMatrix, matrix);

        matrix = mult(prev_matrix, matrix);
        var matrix_final = mult(lookAtMatrix, matrix);
        
        // Set next part as base matrix (if num > 1)
        var matrix_2 = mat4();
        matrix_2 = mult(translationMatrix, matrix_2);
        matrix_2 = mult(rotationMatrix, matrix_2);

        prev_matrix = mult(prev_matrix, matrix_2);

        gl.uniformMatrix4fv( matrixLocation, false, flatten(matrix_final) );

        configColors(color_index);

        // Drawing all
        gl.drawArrays( primitiveType, offset, block_arr[1] );
    }
    return prev_matrix;
}
  
/*
Draw body for rabbit function.

Height: Height of Body
Width: Witdh of Body
Thick: Thickness of Body
Basematrix: base_body
*/
function drawRabbitBody(height, width, thick, basematrix) {
    var block_arr = drawBlock(height, width, thick);
    prev_matrix = basematrix;

    translation = block_arr[2];

    // Points to buffer
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(block_arr[0]), gl.STATIC_DRAW);

    // Converting matrixes
    matrix = mat4();
    var rotMatDiagonal = rotate(-20, [0,0,1]);

    // Set Angle Slider - Human Body
    generateSliderAngles(angle*2-30, idx_rabbit_body, [0.2,1,0]);

    matrix = mult(rotMatDiagonal, matrix);
    matrix = mult(rotationMatrix, matrix);
    matrix = mult(prev_matrix, matrix);
    prev_matrix = matrix;
    var matrix_final = mult(lookAtMatrix, matrix);

    gl.uniformMatrix4fv( matrixLocation, false, flatten(matrix_final) );

    configColors(idx_rabbit_body);

    // Drawing all
    gl.drawArrays( primitiveType, offset, block_arr[1] );
    return prev_matrix;
}
    