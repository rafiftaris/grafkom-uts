/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 2: Hierarchical Modelling & Lighting
Worksheet 3: Texturing & Shadowing

Functions to generate matrices on hierarchichal model.
*/

/*
Var to keep the colors according to bodyPart index

First array is when lights on
Second array is when lights off

First index is ambient
Second index is diffuse
*/
var hexColors = [
    [
        [],
        []
    ], // 0 = rlhead (not used, 1 color for head)
    [
        ["#00a18e", "#20bdaa"],
        ["#072420", "#0d403a"]
    ], // 1 = udhead
    [
        ["#954000", "#E0841D"],
        ["#2E1400", "#59350D"]
    ], // 2 = body
    [
        ["#018515", "#47ff63"],
        ["#003007", "#1d5e27"]
    ], // 3 = lua & rabbit tail
    [
        ["#a5ad15", "#f3fa75"],
        ["#53570a", "#727538"]
    ], // 4 = lla
    [
        ["#018515", "#47ff63"],
        ["#003007", "#1d5e27"]
    ], // 5 = rua
    [
        ["#a5ad15", "#f3fa75"],
        ["#53570a", "#727538"]
    ], // 6 = rla
    [
        ["#2227bd", "#7d81ff"],
        ["#0f114d", "#3d3f75"]
    ], // 7 = rl
    [
        ["#2227bd", "#7d81ff"],
        ["#0f114d", "#3d3f75"]
    ], // 8 = ll

    // Rabbit
    [
        ["#a5ad15", "#f3fa75"],
        ["#53570a", "#727538"]
    ], // 9 = lear
    [
        ["#a5ad15", "#f3fa75"],
        ["#53570a", "#727538"]
    ], // 10 = rear
    [
        ["#00a18e", "#20bdaa"],
        ["#072420", "#0d403a"]
    ], // 11 = head
    [
        ["#954000", "#E0841D"],
        ["#2E1400", "#59350D"]
    ], // 12 = body
    [
        ["#2227bd", "#7d81ff"],
        ["#0f114d", "#3d3f75"]
    ], // 13 = lhand
    [
        ["#2227bd", "#7d81ff"],
        ["#0f114d", "#3d3f75"]
    ], // 14 = rhand

    [
        ["#2227bd", "#7d81ff"],
        ["#0f114d", "#3d3f75"]
    ], // 15 = lul
    [
        ["#5e0d91", "#a35ad1"],
        ["#32044f", "#55316b"]
    ], // 16 = lll
    [
        ["#2227bd", "#7d81ff"],
        ["#0f114d", "#3d3f75"]
    ], // 17 = rul
    [
        ["#5e0d91", "#a35ad1"],
        ["#32044f", "#55316b"]
    ], // 18 = rll
    [
        ["#018515", "#47ff63"],
        ["#003007", "#1d5e27"]
    ], // 19 = tail
];

/*
Function to configure colors based on
index of each part.

Colors are set in hexColors var above.
*/
function configColors(index_part){
    var colorSet = hexColors[index_part];
    var ambientCol, diffuseCol;

    // Taking the colors assigned
    if(lights_on){
        ambientCol = colorSet[0][0];
        diffuseCol = colorSet[0][1];
    } else {
        ambientCol = colorSet[1][0];
        diffuseCol = colorSet[1][1];
    }

    // Pass to make materials
    configNAD(ambientCol, diffuseCol);
}

/*
Normal, Ambient, and Diffuse Config Function

ambientColor: Ambient Color in Hex
diffuseColor: Diffuse Color in Hex

Configure normal matrix, ambient matrix, and diffuse matrix when drawing objects
*/
function configNAD(ambientColor, diffuseColor){
    // Normal Matrix
    var normalMatrix = [
        vec3(matrix[0][0], matrix[0][1], matrix[0][2]),
        vec3(matrix[1][0], matrix[1][1], matrix[1][2]),
        vec3(matrix[2][0], matrix[2][1], matrix[2][2])
    ];
    gl.uniformMatrix3fv(normalMatrixLoc, false, flatten(normalMatrix) );

    // Change Color from ambient
    var lightAmbient = createVec4(1.0);
    var materialAmbient = hexToRgb(ambientColor);
    ambientProduct = mult(lightAmbient, materialAmbient);
    gl.uniform4fv( ambientProductLoc, flatten(ambientProduct ));

    // Change Color from diffuse
    var lightDiffuse = createVec4(1.0);
    var materialDiffuse = hexToRgb(diffuseColor);
    diffuseProduct = mult(lightDiffuse, materialDiffuse);
    gl.uniform4fv( diffuseProductLoc, flatten(diffuseProduct) );

};

/*
Function to create the rotation matrix.
If animate is on use anim angle,
otherwise use the manual angle from slider.
*/
function setRotateMatrix(anim_angle, manual_angle, sumbu) {
    if (animate_on) {
        rotationMatrix = rotate(anim_angle, sumbu);
    } else {
        rotationMatrix = rotate(manual_angle, sumbu);
    }
}

/*
Function to create the rotation matrix
and save the current angle to the slider_angles array.

curAngle: is the angle to be rotated
bodyPartIndex: is the index number of the body part
               to be rotated
sumbu: the rotation axis
*/
function generateSliderAngles(curAngle, bodyPartIndex, sumbu){
    anim_angle = curAngle;
    manual_angle = slider_angles[bodyPartIndex];
    setRotateMatrix(anim_angle, manual_angle, sumbu);
    if (animate_on){
        slider_angles[bodyPartIndex] = anim_angle;
    }
    return rotationMatrix;
}