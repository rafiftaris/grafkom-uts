/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 2: Hierarchical Modelling & Lighting
Worksheet 3: Texturing & Shadowing

Array Manipulation file consist of functions to help
the process of creating vertices and arrays.
*/

// Add Z to [X,Y] array
function addZ(vertex, z) {
    return [vertex[0], vertex[1], z];
}

// Create Vec4 with same x,y,z and alpha = 1.0
function createVec4(faktor) {
    return vec4(faktor, faktor, faktor, 1.0);
}  

// Draw Rect with Width and Height
function draw2DRect(width, height) {
    var points_tmp = [];
    points_tmp.push([-width/2, -height/2]);
    points_tmp.push([ width/2, -height/2]);
    points_tmp.push([ width/2,  height/2]);
    points_tmp.push([-width/2,  height/2]);
    return points_tmp;
}

/*
Function to generate triangle points to be drawn
from a set of vertices.

Input: Array of 3d vertices in order
Output: Ordered points (vec3) in triangle order
Example Out: v0, v1, v2, v1, v2, v3, etc.
*/
function genTriangles(vertices) {
    var ret = [];
    for (var i = 0; i < vertices.length-2; i++) {
        ret.push(vertices[0]);
        ret.push(vertices[i+1]);
        ret.push(vertices[i+2]);
    }
    return ret;
}

/*
Function to generate block points to be drawn
from a set of vertices.

Input: Array of 2d vertices (front surface) and the thickness
Output: Ordered points (vec3) in triangle order
*/
function genBlockVertices(vertex2d, thickness) {
    var points_tmp = [];

    // Create first four vertices in 3D (front side)
    var vertex3d = [];
    for (var i = 0; i < vertex2d.length; i++) {
        vertex3d[i] = addZ(vertex2d[i], thickness/2);
    }
    points_tmp = genTriangles(vertex3d); // front side
    
    // Create next four vertices in 3D (back side)
    var vertex3d_2 = [];
    for (var i = 0; i < vertex2d.length; i++) {
        vertex3d_2[i] = addZ(vertex2d[i], - thickness/2);
    }
    points_tmp = points_tmp.concat(genTriangles(vertex3d_2)); // back side

    // Create the rest 4 sides of block, auto generated in loop
    for (var i = 0; i < vertex3d.length; i++) {
        var next = i+1;
        if (i+1 >= vertex3d.length) {
            next = 0;
        }
        var sisi = [
            vertex3d[i], vertex3d_2[i],
            vertex3d_2[next], vertex3d[next]
        ];
        points_tmp = points_tmp.concat(genTriangles(sisi));
    }
    return points_tmp;
}

/*
Function to open array of vertices (vec3) 
to a 1-dimensional array of floats.
*/
function openVertices(arr) {
    var arr_tmp = [];
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].length; j++) {
            arr_tmp.push(arr[i][j]);
        }
    }
    return arr_tmp;
}

/*
Draw a block with square base.

Output:
Index 0 - The vertices in float array.
Index 1 - The number of vertices.
Index 2 - The translation matrix to move origin.
*/
function drawSquareBlock(len, width) {
    var rect = draw2DRect(width, len);
    var vertices = openVertices(genBlockVertices(rect, width));
    return [vertices, vertices.length/3, [0, len/2, 0]];
}

/*
Draw a block. Similar with drawSquareBlock
but have thickness that can be set.

Output:
Index 0 - The vertices in float array.
Index 1 - The number of vertices.
Index 2 - The translation matrix to move origin.
*/
function drawBlock(len, width, thick) {
    var rect = draw2DRect(width, len);
    var vertices = openVertices(genBlockVertices(rect, thick));
    return [vertices, vertices.length/3, [0, len/2, 0]];
}
