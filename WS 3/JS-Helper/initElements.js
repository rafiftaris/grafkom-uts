/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 2: Hierarchical Modelling & Lighting
Worksheet 3: Texturing & Shadowing

Functions to initialize JS Elements.
*/

// lights flag
var lights_on = true;

// animation flag
var animate_on = true;

/*
Function to initialize button elements in HTML.
Add event listeners to these buttons.
*/
function initButtonElements(){
    // Animate Button
    var ani = document.getElementById("ButtonAnimate");
    ani.addEventListener("click", function(){
        if(animate_on){
            animate_on = false;
            ani.innerHTML = "Restart Animation";
        }
        else{
            angle = 0;
            animate_on = true;
            ani.innerHTML = "Stop Animation";
        }
    });

    // Animate Button
    var light = document.getElementById("ButtonLight");
    light.addEventListener("click", function(){
        if(lights_on){
            lights_on = false;
            light.innerHTML = "Lights On";
        }
        else{
            lights_on = true;
            light.innerHTML = "Lights Out";
        }
    });


}