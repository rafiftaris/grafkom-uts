"use strict";
/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 2: Hierarchical Modelling & Lighting
Worksheet 3: Texturing & Shadowing

Main Program for the Man and Rabbit Model.
Run this HTML in browser to see the model.
*/

/* ============== STANDARD WebGL Vars ============== */
// Taken from Web-GL Master

var canvas;
var gl;

// Programs
var program;
var skybox_program;

var primitiveType;

var colorUniformLocation;
var angle = 0;
var angleInRadians = 0;
var matrix;
var matrixLocation;
var translationMatrix;
var rotationMatrix;
var projectionMatrix;

var middlewidth = 0;
var middleheight = 0;

var z_fight_num = 0.1;

var normalMatrixLoc;
var inc_angle;
var lightPosition = vec4(-60.0, 60.0, 10.0, 0.0); // Light position

var ambientProduct;
var diffuseProduct;
var specularProduct;
var ambientProductLoc;
var diffuseProductLoc;

var viewDirectionProjectionInverseMatrix;

/* ============== CUSTOM VARS ============== */

// List of slider and text id's
var lst_of_slider = ["s1","s2","s3","s4","s5","s6","s7","s8","s9","s10",
                      "s11","s12","s13","s14","s15","s16","s17","s18","s19"];
var lst_of_content = ["c1","c2","c3","c4","c5","c6","c7","c8","c9","c10",
                      "c11","c12","c13","c14","c15","c16","c17","c18","c19"];

// Text to be displayed in order of each part id's
var slider_content = [
    "Kepala (kiri - kanan): ",  // 0
    "Kepala (atas - bawah): ", // 1
    "Badan: ",  // 2
    "Bahu Kanan: ",  // 3
    "Lengan Kanan: ",  // 4
    "Bahu Kiri: ", // 5
    "Lengan Kiri: ",  // 6
    "Kaki Kanan: ",  // 7
    "Kaki Kiri: ",  // 8

    "Kuping Kiri: ",  // 9
    "Kuping Kanan: ", // 10
    "Kepala (atas - bawah): ", // 11
    "Badan: ", // 12
    "Tangan Kiri: ", // 13
    "Tangan Kanan: ", // 14
    "Paha Kiri: ", // 15
    "Kaki Kiri: ", // 16
    "Paha Kanan: ", // 17
    "Kaki Kanan: " // 18
];

// Value for each slider angles
var slider_angles = [
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0
];

// Indexes for each part
var idx_human_rlhead = 0;
var idx_human_udhead = 1;
var idx_human_body = 2;
var idx_human_lua = 3;
var idx_human_lla = 4;
var idx_human_rua = 5;
var idx_human_rla = 6;
var idx_human_ll = 8;
var idx_human_rl = 7;

var idx_rabbit_lear = 9;
var idx_rabbit_rear = 10;
var idx_rabbit_head = 11;
var idx_rabbit_body = 12;
var idx_rabbit_lhand = 13;
var idx_rabbit_rhand = 14;
var idx_rabbit_lul = 15;
var idx_rabbit_lll = 16;
var idx_rabbit_rul = 17;
var idx_rabbit_rll = 18;
var idx_rabbit_tail = 19;

// Main Function
window.onload = function init() {
    canvas = document.getElementById( "gl-canvas" );

    // Initialize buttons
    initButtonElements();

    // Configure WebGL
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.1, 0.1, 0.1, 1.0 );

    // Create programs
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    // skybox_program = initShaders(gl,"skybox-vertex-shader","skybox-fragment-shader");

    // Init increment angle
    inc_angle = 0.3;
    render();
    
    // let our quad pass the depth test at 1.0
    gl.depthFunc(gl.LEQUAL);

    // gl.useProgram(skybox_program);

}

// Camera setup function
function cameraSetup(){
    // camera going in circle 2 units from origin looking at origin
    var cameraPosition = [0, 0, 0];
    var target = [0, 0, 0];
    var up = [0, 1, 0];
    // Compute the camera's matrix using look at.
    var cameraMatrix = lookAt(cameraPosition, target, up);

    // Make a view matrix from the camera matrix.
    var viewMatrix = inverse(cameraMatrix);

    // We only care about direciton so remove the translation
    var viewDirectionMatrix = viewMatrix;

    var viewDirectionProjectionMatrix = mult(
        projectionMatrix, viewDirectionMatrix);
    viewDirectionProjectionInverseMatrix =
        inverse(viewDirectionProjectionMatrix);
}

// Function to init shader of figure objects
function initShaderFigure(){
    // Use the default depth test
    gl.depthFunc(gl.LESS);  

    // Load shaders and initialize attribute buffers
    gl.useProgram( program );

    // Load the data into the GPU
    var letterbuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, letterbuffer );

    // Associate out shader variables with our data buffer
    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
    var vNormal = gl.getAttribLocation( program, "vNormal" );
    gl.vertexAttribPointer( vNormal, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vNormal);

    colorUniformLocation = gl.getUniformLocation(program, "u_color");

    matrixLocation = gl.getUniformLocation(program, "modelViewMatrix");
    middlewidth = Math.floor(gl.canvas.width/2);
    middleheight = Math.floor(gl.canvas.height/2);

    // Primitive type to be drawn
    primitiveType = gl.TRIANGLES;

    // Compute the projection matrix
    projectionMatrix = ortho(-middlewidth, middlewidth, -middleheight, middleheight, -middlewidth, middlewidth);
    projectionMatrix = perspective(45.0, gl.canvas.width/gl.canvas.height, 1, 10000);
    gl.uniformMatrix4fv( gl.getUniformLocation(program, "projectionMatrix"), false, flatten(projectionMatrix));

    // Init Light Materials
    var lightAmbient = createVec4(1.0);
    var lightDiffuse = createVec4(1.0);
    var lightSpecular = createVec4(0.4);

    var materialAmbient = hexToRgb("#23527e");
    var materialDiffuse = hexToRgb("#3c8ac2");
    var materialSpecular = hexToRgb("#fff785");
    var materialShininess = 400.0;

    ambientProduct = mult(lightAmbient, materialAmbient);
    diffuseProduct = mult(lightDiffuse, materialDiffuse);
    specularProduct = mult(lightSpecular, materialSpecular);

    normalMatrixLoc = gl.getUniformLocation( program, "normalMatrix" );
    ambientProductLoc = gl.getUniformLocation(program, "ambientProduct"); // Ambient can change
    diffuseProductLoc = gl.getUniformLocation(program, "diffuseProduct"); // Diffuse can change

    gl.uniform4fv( gl.getUniformLocation(program, "specularProduct"),flatten(specularProduct));
    gl.uniform4fv( gl.getUniformLocation(program, "lightPosition"), flatten(lightPosition ));
    gl.uniform1f( gl.getUniformLocation(program, "shininess"),materialShininess );
}

// // Function to init shader of skybox
// function initShaderSkybox(){
//     // let our quad pass the depth test at 1.0
//     gl.depthFunc(gl.LEQUAL);

//     gl.useProgram(skybox_program);

//     // Load the data into the GPU
//     var letterbuffer = gl.createBuffer();
//     gl.bindBuffer( gl.ARRAY_BUFFER, letterbuffer );

//     // Lookup uniform location
//     var skyboxLocation = gl.getUniformLocation(program, "uSkybox");
//     var viewDirectionProjectionInverseLocation = 
//         gl.getUniformLocation(program, "uViewDirectionProjectionInverse");

//     // Set the uniforms
//     gl.uniformMatrix4fv(
//         viewDirectionProjectionInverseLocation, false, 
//         viewDirectionProjectionInverseMatrix);
 
//     // Tell the shader to use texture unit 0 for u_skybox
//     gl.uniform1i(skyboxLocation, 0);
// }

// Main render function
function render() {

    // Resizing Canvas
    resize(gl.canvas);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    gl.clear( gl.COLOR_BUFFER_BIT );
    
    // Get input from slider
    for(var i=0; i<19; i++){
        var temp = 0;
        var temp2 = 0;
        temp = document.getElementById(lst_of_slider[i]).value;
        temp2 = document.getElementById(lst_of_content[i]);
        temp2.innerHTML = slider_content[i] + temp.toString();
        
        if(animate_on){
            document.getElementById(lst_of_slider[i]).value = slider_angles[i];
        } else {
            slider_angles[i] = temp;
        }
    }
    
    // Rotate angles auto
    if(animate_on) {
        if (angle >= 180/3) {
            inc_angle = -0.5;
        } else if (angle <= 0) {
            inc_angle = 0.5;
        }
        angle += inc_angle;
    }

    gl.enable(gl.DEPTH_TEST);

    // Clear the canvas AND the depth buffer.
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    initShaderFigure();

    // Create Light
    drawLight(2, mat4());

    // Draw Figures
    drawMan();
    drawRabbit();
    
    // Environment Setup
    cameraSetup();
    // initShaderSkybox();
    // drawSkyBox();

    // Loop Render
    requestAnimationFrame(render);
}
