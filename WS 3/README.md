# Work Sheet 2 & 3 Grafika Komputer Genap 2019/2020

# HUMAN and RABBIT

## How to run
Untuk menjalankan program kami, klik 2x manAndRabbit.html, atau bisa dengan cara membuka browser kesukaan anda dan mendrag and drop manAndRabbit.html ke dalam browser tersebut.

## DOKUMENTASI
File-file berkaitan dengan dokumentasi berada didalam folder "Dokumentasi". Folder tersebut berisikan file ModelParts.txt, Modules.txt, ProgramFlow.txt 

### ModelParts.txt:
Pada file ini menjelaskan keterhubungan antar objek yang ada. Keterhubungan yang dimaksud adalah objek lain yang harus dipertimbangkan jika kita ingin menggerakan suatu objek.

### Modules.txt 
Pada file ini menjelaskan file-file yang telah kami buat. Penjelasan akan menggambarkan bagaimana tiap-tiap file ini berkerja untuk bisa menjadi suatu kesatuan program.

### ProgramFlow.txt 
Pada file ini menjelaskan alur jalannya program. Mulai dari membuat canvas di html sampai membuat animasi yang bisa bergerak otomatis. 

## Contributor
### Kelompok Anum
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253
- Raihansyah Athallah Andrian - 1706040196