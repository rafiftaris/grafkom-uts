# Work Sheet 3 Grafika Komputer Genap 2019/2020

# HUMAN and RABBIT Scene

## How to run
Untuk menjalankan program kami, jalankan __http-server__ pada direktori folder. Sebelumnya sudah harus menginstall npm dan menggunakan command __npm install --global http-server__. Untuk menggerakkan kamera gunakan W untuk bergerak maju, S untuk bergerak mundur, A untuk bergerak ke kiri, D untuk bergerak ke kanan, Up arrow untuk menggerakkan keatas, down arrow untuk menggerakkan ke bawah, right arrow untuk rotatae kanan dan left arrow untuk rotate kiri.

Untuk instalasi npm bisa dilakukan pada link berikut: https://www.npmjs.com/get-npm

## DOKUMENTASI
File-file berkaitan dengan dokumentasi berada didalam folder __Dokumentasi__. Folder tersebut berisikan file ModelParts.txt, Modules.txt, ProgramFlow.txt 

### ModelParts.txt:
Pada file ini menjelaskan keterhubungan antar objek yang ada. Keterhubungan yang dimaksud adalah objek lain yang harus dipertimbangkan jika kita ingin menggerakan suatu objek.

### Modules.txt 
Pada file ini menjelaskan file-file yang telah kami buat. Penjelasan akan menggambarkan bagaimana tiap-tiap file ini berkerja untuk bisa menjadi suatu kesatuan program.

### ProgramFlow.txt 
Pada file ini menjelaskan alur jalannya program. Mulai dari membuat canvas di html sampai membuat animasi yang bisa bergerak otomatis. 

### Changes from WS2
Due to the difference in how we create objects in WS2 it is extremely hard to apply all the requirements for WS3, we have decided to make major changes since we want to create a scene in WS3. And since we make a lot of changes unfortunately for the time being we were not able to animate the hierarchical model. We already created a branch that animates it but were not successful in merging it with the shadow and texture part.

## Contributor - Kelompok Anum
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253
- Raihansyah Athallah Andrian - 1706040196