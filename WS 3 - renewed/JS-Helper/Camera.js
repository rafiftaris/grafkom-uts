'use strict';
/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 3: Texturing & Shadowing

Functions to create camera and move around.
*/

/*
Camera Object to be used in scene.
*/
var Camera = function (pos, lookAt, up) {
	this.forward = vec3.create(); // Direction
	this.up = vec3.create();
	this.right = vec3.create();

	this.position = pos;

	vec3.subtract(this.forward, lookAt, this.position); // Get direction
	vec3.cross(this.right, this.forward, up); // Orthogonal to direction
	vec3.cross(this.up, this.right, this.forward); // Orthogonal up

    // Normalizing
	vec3.normalize(this.forward, this.forward);
	vec3.normalize(this.right, this.right);
	vec3.normalize(this.up, this.up);
};

/*
Function to get the view matrix of camera.
*/
Camera.prototype.GetViewMatrix = function (out) {
	var lookAt = vec3.create();
	vec3.add(lookAt, this.position, this.forward);
	mat4.lookAt(out, this.position, lookAt, this.up);
	// console.log(this.position)
	return out;
};

/*
Funcion to rotate camera right
*/
Camera.prototype.rotateRight = function (rad) {
	var rightMatrix = mat4.create();
	mat4.rotate(rightMatrix, rightMatrix, rad, vec3.fromValues(0, 0, 1));
	vec3.transformMat4(this.forward, this.forward, rightMatrix);
	this._realign();
};

/*
Funcion to realigning the camera
*/
Camera.prototype._realign = function() {
	vec3.cross(this.right, this.forward, this.up);
	vec3.cross(this.up, this.right, this.forward);

	vec3.normalize(this.forward, this.forward);
	vec3.normalize(this.right, this.right);
	vec3.normalize(this.up, this.up);
};

/*
Funcion to move camera forward (y-axis)
*/
Camera.prototype.moveForward = function (dist) {
	vec3.scaleAndAdd(this.position, this.position, this.forward, dist);
};

/*
Funcion to move camera right (x-axis)
*/
Camera.prototype.moveRight = function (dist) {
	vec3.scaleAndAdd(this.position, this.position, this.right, dist);
};

/*
Funcion to move camera up (z-axis)
*/
Camera.prototype.moveUp = function (dist) {
	vec3.scaleAndAdd(this.position, this.position, this.up, dist);
};