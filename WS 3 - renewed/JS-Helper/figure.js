'use strict';

/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 3: Texturing & Shadowing

Hierarcy Setup Functions.
Adapted from WS 2
*/

// Format vertex: (x, z blender, -y blender)
var human_body_base = vec3.fromValues(2.10621, 1.19453, -0.615895);
var human_head_connected = vec3.fromValues(2.10632,1.79737,-0.623392);
var human_RUA_connected = vec3.fromValues(1.76955,1.39837,-0.617137);
var human_RLA_connected = vec3.fromValues(1.76933,1.28141,-0.67779);
var human_LUA_connected = vec3.fromValues(2.44181,1.40631,-0.613453);
var human_LLA_connected = vec3.fromValues(2.44159,1.29074,-0.67549);
var human_right_leg_connected = vec3.fromValues(1.96897,0.75435,-0.618088);
var human_left_leg_connected = vec3.fromValues(2.2536,0.75435,-0.618445);

var rabbit_body_base = vec3.fromValues(-0.055149, 0.524755, -0.505686);
var rabbit_head_connected = vec3.fromValues(0.143569,0.990397,-0.504478);
var rabbit_right_ear_connected = vec3.fromValues(0.129209,1.22728,-0.569453);
var rabbit_left_ear_connected = vec3.fromValues(0.129209,1.22728,-0.427713);
var rabbit_left_arm_connected = vec3.fromValues(0.300368,0.438656,-0.708625);
var rabbit_right_arm_connected = vec3.fromValues(0.300368,0.438656,-0.302778);
var rabbit_LUL_connected = vec3.fromValues(-0.113029,0.296821,-0.742995);
var rabbit_LLL_connected = vec3.fromValues(-0.017346,0.152869,-0.744371);
var rabbit_RUL_connected = vec3.fromValues(-0.113029,0.296821,-0.256014);
var rabbit_RLL_connected = vec3.fromValues(-0.017346,0.152869,-0.25739);
var rabbit_tail_connected = vec3.fromValues(-0.406019,0.332001,-0.49393);

var truck_body_base = vec3.fromValues(-3.13175,0.533342,-2.80009);
var truck_flwheel_connected = vec3.fromValues(-2.89003,0.222814,-2.37275);
var truck_frwheel_connected = vec3.fromValues(-3.39112,0.222814,-2.38551);
var truck_head_connected = vec3.fromValues(-3.11921,0.500798,-2.06347);
var truck_laser_left_connected = vec3.fromValues(-2.88023,0.52747,-1.90659);
var truck_laser_right_connected = vec3.fromValues(-3.35201,0.52747,-1.90659);
var truck_rlwheel_connected = vec3.fromValues(-2.89003,0.222814,-3.24886);
var truck_rrwheel_connected = vec3.fromValues(-3.39112,0.222814,-3.24886);

var base = mat4.create();

// Rotation degree
var human_body_rotation = 0;
var human_body_direction = 1;
var human_head_rotation = 0;
var human_head_direction = 1;
var human_RUA_rotation = 0;
var human_RUA_direction = 1;
var human_LUA_rotation = 0;
var human_LUA_direction = -1;
var human_RLA_rotation = 0;
var human_RLA_direction = 1;
var human_LLA_rotation = 0;
var human_LLA_direction = -1;
var human_right_leg_rotation = 0;
var human_right_leg_direction = -1;
var human_left_leg_rotation = 0;
var human_left_leg_direction = 1;

var rabbit_body_rotation = 0;
var rabbit_body_direction = -1;
var rabbit_head_rotation = 0;
var rabbit_head_direction = 1;
var rabbit_right_ear_rotation = 0;
var rabbit_right_ear_direction = 1;
var rabbit_left_ear_rotation = 0;
var rabbit_left_ear_direction = 1;
var rabbit_right_arm_rotation = 0;
var rabbit_right_arm_direction = 1;
var rabbit_left_arm_rotation = 0;
var rabbit_left_arm_direction = 1;
var rabbit_LUL_rotation = 0;
var rabbit_LUL_direction = -1;
var rabbit_RUL_rotation = 0;
var rabbit_RUL_direction = -1;
var rabbit_LLL_rotation = 0;
var rabbit_LLL_direction = -1;
var rabbit_RLL_rotation = 0;
var rabbit_RLL_direction = -1;
var rabbit_tail_rotation = 0;
var rabbit_tail_direction = 1;

var truck_body_rotation = 0;
var truck_body_translation_direction = 1;
var truck_body_rotation_direction = -1;
var truck_body_translation = 0;
var truck_flwheel_rotation = 0;
var truck_flwheel_direction = 1;
var truck_frwheel_rotation = 0;
var truck_frwheel_direction = 1;
var truck_head_rotation = 0;
var truck_head_direction = 1;
var truck_laser_left_rotation = 0;
var truck_laser_left_direction = -1;
var truck_laser_right_rotation = 0;
var truck_laser_right_direction = -1;
var truck_rlwheel_rotation = 0;
var truck_rlwheel_direction = 1;
var truck_rrwheel_rotation = 0;
var truck_rrwheel_direction = 1;

var parts;

/*
Degree to Radian Converter
*/
function deg_to_rad(degrees){
    var pi = Math.PI;
    return degrees * (pi/180);
};

/*
Radian to Degree Converter
*/
function rad_to_deg(radians){
    var pi = Math.PI;
    return radians * (180/pi);
};  

/*
Init unresetted variables
*/
MainScene.prototype.InitFigureVars = function(){
    var me = this;
    // Body clock count
    me.human_body_count = 0;
    me.rabbit_body_count = 0;

    // Perpindahan truck
	me.truck_distance_diff = vec3.fromValues(0,0,0);
}

/*
Reset Figures to starting position
*/
MainScene.prototype.ResetFigures = function(){
    var me = this;

    //Reset variables
    // Format vertex: (x, z blender, -y blender)
    human_body_base = vec3.fromValues(2.10621, 1.19453, -0.615895);
    human_head_connected = vec3.fromValues(2.10632,1.79737,-0.623392);
    human_RUA_connected = vec3.fromValues(1.76955,1.39837,-0.617137);
    human_RLA_connected = vec3.fromValues(1.76933,1.28141,-0.67779);
    human_LUA_connected = vec3.fromValues(2.44181,1.40631,-0.613453);
    human_LLA_connected = vec3.fromValues(2.44159,1.29074,-0.67549);
    human_right_leg_connected = vec3.fromValues(1.96897,0.75435,-0.618088);
    human_left_leg_connected = vec3.fromValues(2.2536,0.75435,-0.618445);

    rabbit_body_base = vec3.fromValues(-0.055149, 0.524755, -0.505686);
    rabbit_head_connected = vec3.fromValues(0.143569,0.990397,-0.504478);
    rabbit_right_ear_connected = vec3.fromValues(0.129209,1.22728,-0.569453);
    rabbit_left_ear_connected = vec3.fromValues(0.129209,1.22728,-0.427713);
    rabbit_left_arm_connected = vec3.fromValues(0.300368,0.438656,-0.708625);
    rabbit_right_arm_connected = vec3.fromValues(0.300368,0.438656,-0.302778);
    rabbit_LUL_connected = vec3.fromValues(-0.113029,0.296821,-0.742995);
    rabbit_LLL_connected = vec3.fromValues(-0.017346,0.152869,-0.744371);
    rabbit_RUL_connected = vec3.fromValues(-0.113029,0.296821,-0.256014);
    rabbit_RLL_connected = vec3.fromValues(-0.017346,0.152869,-0.25739);
    rabbit_tail_connected = vec3.fromValues(-0.406019,0.332001,-0.49393);

    truck_body_base = vec3.fromValues(-3.13175,0.533342,-2.80009);
    truck_flwheel_connected = vec3.fromValues(-2.89003,0.222814,-2.37275);
    truck_frwheel_connected = vec3.fromValues(-3.39112,0.222814,-2.38551);
    truck_head_connected = vec3.fromValues(-3.11921,0.500798,-2.06347);
    truck_laser_left_connected = vec3.fromValues(-2.88023,0.52747,-1.90659);
    truck_laser_right_connected = vec3.fromValues(-3.35201,0.52747,-1.90659);
    truck_rlwheel_connected = vec3.fromValues(-2.89003,0.222814,-3.24886);
    truck_rrwheel_connected = vec3.fromValues(-3.39112,0.222814,-3.24886);


    base = mat4.create();

    // Rotation degree
    human_body_rotation = 0;
    human_body_direction = 1;
    human_head_rotation = 0;
    human_head_direction = 1;
    human_RUA_rotation = 0;
    human_RUA_direction = 1;
    human_LUA_rotation = 0;
    human_LUA_direction = -1;
    human_RLA_rotation = 0;
    human_RLA_direction = 1;
    human_LLA_rotation = 0;
    human_LLA_direction = -1;
    human_right_leg_rotation = 0;
    human_right_leg_direction = -1;
    human_left_leg_rotation = 0;
    human_left_leg_direction = 1;

    rabbit_body_rotation = 0;
    rabbit_body_direction = -1;
    rabbit_head_rotation = 0;
    rabbit_head_direction = 1;
    rabbit_right_ear_rotation = 0;
    rabbit_right_ear_direction = 1;
    rabbit_left_ear_rotation = 0;
    rabbit_left_ear_direction = 1;
    rabbit_right_arm_rotation = 0;
    rabbit_right_arm_direction = 1;
    rabbit_left_arm_rotation = 0;
    rabbit_left_arm_direction = 1;
    rabbit_LUL_rotation = 0;
    rabbit_LUL_direction = -1;
    rabbit_RUL_rotation = 0;
    rabbit_RUL_direction = -1;
    rabbit_LLL_rotation = 0;
    rabbit_LLL_direction = -1;
    rabbit_RLL_rotation = 0;
    rabbit_RLL_direction = -1;
    rabbit_tail_rotation = 0;
    rabbit_tail_direction = 1;

    truck_body_rotation = 0;
    truck_body_translation_direction = 1;
    truck_body_rotation_direction = -1;
    truck_body_translation = 0;
    truck_flwheel_rotation = 0;
    truck_flwheel_direction = 1;
    truck_frwheel_rotation = 0;
    truck_frwheel_direction = 1;
    truck_head_rotation = 0;
    truck_head_direction = 1;
    truck_laser_left_rotation = 0;
    truck_laser_left_direction = -1;
    truck_laser_right_rotation = 0;
    truck_laser_right_direction = -1;
    truck_rlwheel_rotation = 0;
    truck_rlwheel_direction = 1;
    truck_rrwheel_rotation = 0;
    truck_rrwheel_direction = 1;


    // Clock count
    me.human_head_count = 0;
    me.human_RUA_count = 0;
    me.human_LUA_count = 0;
    me.human_RLA_count = 0;
    me.human_LLA_count = 0;
    me.human_right_leg_count = 0;
    me.human_left_leg_count = 0;

    me.rabbit_head_count = 0;
    me.rabbit_right_arm_count = 0;
    me.rabbit_left_arm_count = 0;
    me.rabbit_LUL_count = 0;
    me.rabbit_RUL_count = 0;
    me.rabbit_LLL_count = 0;
    me.rabbit_RLL_count = 0;
    me.rabbit_right_ear_count = 0;
    me.rabbit_left_ear_count = 0;
    me.rabbit_tail_count = 0;

    me.truck_body_translation_count = 0;
    me.truck_body_rotation_count = 0;
    me.truck_flwheel_count = 0;
    me.truck_frwheel_count = 0;
    me.truck_head_count = 0;
    me.truck_laser_left_count = 0;
    me.truck_laser_right_count = 0;
    me.truck_rlwheel_count = 0;
    me.truck_rrwheel_count = 0;

    //Reset truck position
    mat4.translate(
        me.TruckBodyMesh.world, me.TruckBodyMesh.world,
        me.truck_distance_diff
    );
    me.truck_distance_diff = vec3.fromValues(0,0,0);
}

/*
Initialize matrices for figure hierarchy
*/
MainScene.prototype.InitFigureMatrices = function () {
    parts = [
        ['HumanBody',this.HumanBodyMesh.world,base],
        ['HumanHead',this.HumanHeadMesh.world,this.HumanBodyMesh.world],
        ['HumanRUA',this.HumanRUAMesh.world,this.HumanBodyMesh.world],
        ['HumanLUA',this.HumanLUAMesh.world,this.HumanBodyMesh.world],
        ['HumanRLA',this.HumanRLAMesh.world,this.HumanRUAMesh.world],
        ['HumanLLA',this.HumanLLAMesh.world,this.HumanLUAMesh.world],
        ['HumanRightLeg',this.HumanRightLegMesh.world,this.HumanBodyMesh.world],
        ['HumanLeftLeg',this.HumanLeftLegMesh.world,this.HumanBodyMesh.world],
        ['RabbitBody',this.RabbitBodyMesh.world,base],
        ['RabbitHead',this.RabbitHeadMesh.world,this.RabbitBodyMesh.world],
        ['RabbitRightArm',this.RabbitRightArmMesh.world,this.RabbitBodyMesh.world],
        ['RabbitLeftArm',this.RabbitLeftArmMesh.world,this.RabbitBodyMesh.world],
        ['RabbitLUL',this.RabbitLULMesh.world,this.RabbitBodyMesh.world],
        ['RabbitRUL',this.RabbitRULMesh.world,this.RabbitBodyMesh.world],
        ['RabbitLLL',this.RabbitLLLMesh.world,this.RabbitLULMesh.world],
        ['RabbitRLL',this.RabbitRRLMesh.world,this.RabbitRULMesh.world],
        ['RabbitRightEar',this.RabbitRightEarMesh.world,this.RabbitHeadMesh.world],
        ['RabbitLeftEar',this.RabbitLeftEarMesh.world,this.RabbitHeadMesh.world],
        ['RabbitTail',this.RabbitTailMesh.world,this.RabbitBodyMesh.world],
        ['TruckBody',this.TruckBodyMesh.world,base],
        ['TruckHead',this.TruckHeadMesh.world,this.TruckBodyMesh.world],
        ['TruckFLWheel',this.TruckFLWheelMesh.world,this.TruckBodyMesh.world],
        ['TruckFRWheel',this.TruckFRWheelMesh.world,this.TruckBodyMesh.world],
        ['TruckRLWheel',this.TruckRLWheelMesh.world,this.TruckBodyMesh.world],
        ['TruckRRWheel',this.TruckRRWheelMesh.world,this.TruckBodyMesh.world],
        ['TruckLeftLaser',this.TruckLaserLeftMesh.world,this.TruckHeadMesh.world],
        ['TruckRightLaser',this.TruckLaserRightMesh.world,this.TruckHeadMesh.world]
    ];

    mat4.identity(base);
};

/*
Connect parts of figure to hierarchy

args: element of parts variable above
dt: delta time
*/
MainScene.prototype.connectParts = function (args,dt) {
    var parts = args[0];
    var current_matrix = args[1];
    var basematrix = args[2];
    var vector;
    var vector_restore = vec3.create();
    var me = this;

    switch(parts){
        case 'HumanBody':
            human_body_rotation = dt / 1000 * 2 * Math.PI * 0.3;
            me.human_body_count += dt*0.3/10;
            me.human_body_count = me.human_body_count % 100;

            vector = human_body_base;
            vec3.scale(vector_restore,human_body_base,-1);
            
            // Translate poros ke pusat badan
            mat4.translate(
                this.HumanBodyMesh.world, this.HumanBodyMesh.world,
                vector
            );
            // Rotate di pusat badan
            mat4.rotateY(
                this.HumanBodyMesh.world, this.HumanBodyMesh.world,
                human_body_rotation
            );
            // Translate balik
            mat4.translate(
                this.HumanBodyMesh.world, this.HumanBodyMesh.world,
                vector_restore
            );

            // Multiply children
            mat4.multiply(this.HumanHeadMesh.world,basematrix,current_matrix);
            mat4.multiply(this.HumanRUAMesh.world,basematrix,current_matrix);
            mat4.multiply(this.HumanLUAMesh.world,basematrix,current_matrix);
            mat4.multiply(this.HumanRightLegMesh.world,basematrix,current_matrix);
            mat4.multiply(this.HumanLeftLegMesh.world,basematrix,current_matrix);
            break;
        
        case 'HumanHead':
            var speed = 0.5;
            if(me.human_head_count>=500){
                human_head_direction = -1;
            }
            if (me.human_head_count<=-500){
                human_head_direction = 1;
            }
            human_head_rotation += human_head_direction*speed*dt/1000;
            me.human_head_count += human_head_direction*dt;

            vector = human_head_connected;
            vec3.scale(vector_restore,human_head_connected,-1);
            
            mat4.translate(
                this.HumanHeadMesh.world, this.HumanHeadMesh.world,
                vector
            );
            mat4.rotateZ(
                this.HumanHeadMesh.world, this.HumanHeadMesh.world,
                human_head_rotation
            );
            mat4.translate(
                this.HumanHeadMesh.world, this.HumanHeadMesh.world,
                vector_restore
            );

            break;

        case 'HumanRUA':
            var speed = 1.5;
            if(me.human_RUA_count>=800){
                human_RUA_direction = -1;
            }
            if (me.human_RUA_count<=-800){
                human_RUA_direction = 1;
            }
            human_RUA_rotation += human_RUA_direction*speed*dt/1000;
            me.human_RUA_count += human_RUA_direction*dt;

            vector = human_RUA_connected;
            vec3.scale(vector_restore,human_RUA_connected,-1);
            
            mat4.translate(
                this.HumanRUAMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.HumanRUAMesh.world, current_matrix,
                human_RUA_rotation
            );
            mat4.translate(
                this.HumanRUAMesh.world, current_matrix,
                vector_restore
            );
            
            mat4.multiply(this.HumanRLAMesh.world,base,current_matrix);
            break;

        case 'HumanLUA':
            var speed = 1.5;
            if(me.human_LUA_count<=-800){
                human_LUA_direction = 1;
            }
            if (me.human_LUA_count>=800){
                human_LUA_direction = -1;
            }
            human_LUA_rotation += human_LUA_direction*speed*dt/1000;
            me.human_LUA_count += human_LUA_direction*dt;

            vector = human_LUA_connected;
            vec3.scale(vector_restore,human_LUA_connected,-1);
            
            mat4.translate(
                this.HumanLUAMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.HumanLUAMesh.world, current_matrix,
                human_LUA_rotation
            );
            mat4.translate(
                this.HumanLUAMesh.world, current_matrix,
                vector_restore
            );
            
            mat4.multiply(this.HumanLLAMesh.world,base,current_matrix);
            break;

        case 'HumanRLA':
            var speed = 1.5;
            if(me.human_RLA_count>=800){
                human_RLA_direction = -1;
            }
            if (me.human_RLA_count<=-800){
                human_RLA_direction = 1;
            }
            human_RLA_rotation += human_RLA_direction*speed*dt/1000;
            me.human_RLA_count += human_RLA_direction*dt;

            vector = human_RLA_connected;
            vec3.scale(vector_restore,human_RLA_connected,-1);
            
            mat4.translate(
                this.HumanRLAMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.HumanRLAMesh.world, current_matrix,
                human_RLA_rotation
            );
            mat4.translate(
                this.HumanRLAMesh.world, current_matrix,
                vector_restore
            );
            
            break;

        case 'HumanLLA':
            var speed = 1.5;
            if(me.human_LLA_count>=800){
                human_LLA_direction = -1;
            }
            if (me.human_LLA_count<=-800){
                human_LLA_direction = 1;
            }
            human_LLA_rotation += human_LLA_direction*speed*dt/1000;
            me.human_LLA_count += human_LLA_direction*dt;

            vector = human_LLA_connected;
            vec3.scale(vector_restore,human_LLA_connected,-1);
            
            mat4.translate(
                this.HumanLLAMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.HumanLLAMesh.world, current_matrix,
                human_LLA_rotation
            );
            mat4.translate(
                this.HumanLLAMesh.world, current_matrix,
                vector_restore
            );
            
            break;

        case 'HumanRightLeg':
            var speed = 1;
            if(me.human_right_leg_count>=800){
                human_right_leg_direction = -1;
            }
            if (me.human_right_leg_count<=-800){
                human_right_leg_direction = 1;
            }
            human_right_leg_rotation += human_right_leg_direction*speed*dt/1000;
            me.human_right_leg_count += human_right_leg_direction*dt;
            vector = human_right_leg_connected;
            vec3.scale(vector_restore,human_right_leg_connected,-1);
            
            mat4.translate(
                this.HumanRightLegMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.HumanRightLegMesh.world, current_matrix,
                human_right_leg_rotation
            );
            mat4.translate(
                this.HumanRightLegMesh.world, current_matrix,
                vector_restore
            );
                
            break;

        case 'HumanLeftLeg':
            var speed = 1;
            if(me.human_left_leg_count>=800){
                human_left_leg_direction = -1;
            }
            if (me.human_left_leg_count<=-800){
                human_left_leg_direction = 1;
            }
            human_left_leg_rotation += human_left_leg_direction*speed*dt/1000;
            me.human_left_leg_count += human_left_leg_direction*dt;
            vector = human_left_leg_connected;
            vec3.scale(vector_restore,human_left_leg_connected,-1);
            
            mat4.translate(
                this.HumanLeftLegMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.HumanLeftLegMesh.world, current_matrix,
                human_left_leg_rotation
            );
            mat4.translate(
                this.HumanLeftLegMesh.world, current_matrix,
                vector_restore
            );

            break;

        case 'RabbitBody':
            mat4.multiply(this.RabbitBodyMesh.world,current_matrix,basematrix);
            me.rabbit_body_count += dt*0.3/10;
            me.rabbit_body_count = me.rabbit_body_count % 100;

            vector = rabbit_body_base;
            vec3.scale(vector_restore,rabbit_body_base,-1);
            
            mat4.translate(
                this.RabbitBodyMesh.world, this.RabbitBodyMesh.world,
                vector
            );
            mat4.rotateY(
                this.RabbitBodyMesh.world, this.RabbitBodyMesh.world,
                dt / 1000 * 2 * Math.PI * -0.3
            );
            mat4.translate(
                this.RabbitBodyMesh.world, this.RabbitBodyMesh.world,
                vector_restore
            );

            // Multiply children
            mat4.multiply(this.RabbitHeadMesh.world,basematrix,current_matrix);
            mat4.multiply(this.RabbitRULMesh.world,basematrix,current_matrix);
            mat4.multiply(this.RabbitLULMesh.world,basematrix,current_matrix);
            mat4.multiply(this.RabbitRightArmMesh.world,basematrix,current_matrix);
            mat4.multiply(this.RabbitLeftArmMesh.world,basematrix,current_matrix);
            mat4.multiply(this.RabbitTailMesh.world,basematrix,current_matrix);
            break;

        case 'RabbitHead':
            var speed = 0.5;
            if(me.rabbit_head_count>=1000){
                rabbit_head_direction = -1;
            }
            if (me.rabbit_head_count<=-1000){
                rabbit_head_direction = 1;
            }
            rabbit_head_rotation += rabbit_head_direction*speed*dt/1000;
            me.rabbit_head_count += rabbit_head_direction*dt;


            vector = rabbit_head_connected;
            vec3.scale(vector_restore,rabbit_head_connected,-1);
            
            mat4.translate(
                this.RabbitHeadMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitHeadMesh.world, current_matrix,
                rabbit_head_rotation
            );
            mat4.translate(
                this.RabbitHeadMesh.world, current_matrix,
                vector_restore
            );

            mat4.multiply(this.RabbitRightEarMesh.world,base,current_matrix);
            mat4.multiply(this.RabbitLeftEarMesh.world,base,current_matrix);
            break;

        case 'RabbitRightEar':
            var speed = 0.5;
            if(me.rabbit_right_ear_count>=1000){
                rabbit_right_ear_direction = -1;
            }
            if (me.rabbit_right_ear_count<=-1000){
                rabbit_right_ear_direction = 1;
            }
            rabbit_right_ear_rotation += rabbit_right_ear_direction*speed*dt/1000;
            me.rabbit_right_ear_count += rabbit_right_ear_direction*dt;


            vector = rabbit_right_ear_connected;
            vec3.scale(vector_restore,rabbit_right_ear_connected,-1);
            
            mat4.translate(
                this.RabbitRightEarMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitRightEarMesh.world, current_matrix,
                rabbit_right_ear_rotation
            );
            mat4.translate(
                this.RabbitRightEarMesh.world, current_matrix,
                vector_restore
            );
            
            break;

        case 'RabbitLeftEar':
            var speed = 0.5;
            if(me.rabbit_left_ear_count>=1000){
                rabbit_left_ear_direction = -1;
            }
            if (me.rabbit_left_ear_count<=-1000){
                rabbit_left_ear_direction = 1;
            }
            rabbit_left_ear_rotation += rabbit_left_ear_direction*speed*dt/1000;
            me.rabbit_left_ear_count += rabbit_left_ear_direction*dt;


            vector = rabbit_left_ear_connected;
            vec3.scale(vector_restore,rabbit_left_ear_connected,-1);
            
            mat4.translate(
                this.RabbitLeftEarMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitLeftEarMesh.world, current_matrix,
                rabbit_left_ear_rotation
            );
            mat4.translate(
                this.RabbitLeftEarMesh.world, current_matrix,
                vector_restore
            );
            
            break;

        case 'RabbitLeftArm':
            var speed = 0.5;
            if(me.rabbit_left_arm_count>=1000){
                rabbit_left_arm_direction = -1;
            }
            if (me.rabbit_left_arm_count<=-1000){
                rabbit_left_arm_direction = 1;
            }
            rabbit_left_arm_rotation += rabbit_left_arm_direction*speed*dt/1000;
            me.rabbit_left_arm_count += rabbit_left_arm_direction*dt;


            vector = rabbit_left_arm_connected;
            vec3.scale(vector_restore,rabbit_left_arm_connected,-1);
            
            mat4.translate(
                this.RabbitLeftArmMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitLeftArmMesh.world, current_matrix,
                rabbit_left_arm_rotation
            );
            mat4.translate(
                this.RabbitLeftArmMesh.world, current_matrix,
                vector_restore
            );
                
            break;

        case 'RabbitRightArm':
            var speed = 0.5;
            if(me.rabbit_right_arm_count>=1000){
                rabbit_right_arm_direction = -1;
            }
            if (me.rabbit_right_arm_count<=-1000){
                rabbit_right_arm_direction = 1;
            }
            rabbit_right_arm_rotation += rabbit_right_arm_direction*speed*dt/1000;
            me.rabbit_right_arm_count += rabbit_right_arm_direction*dt;


            vector = rabbit_right_arm_connected;
            vec3.scale(vector_restore,rabbit_right_arm_connected,-1);
            
            mat4.translate(
                this.RabbitRightArmMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitRightArmMesh.world, current_matrix,
                rabbit_right_arm_rotation
            );
            mat4.translate(
                this.RabbitRightArmMesh.world, current_matrix,
                vector_restore
            );

            break;

        case 'RabbitRUL':
            var speed = 0.5;
            if(me.rabbit_RUL_count>=1000){
                rabbit_RUL_direction = -1;
            }
            if (me.rabbit_RUL_count<=-1000){
                rabbit_RUL_direction = 1;
            }
            rabbit_RUL_rotation += rabbit_RUL_direction*speed*dt/1000;
            me.rabbit_RUL_count += rabbit_RUL_direction*dt;


            vector = rabbit_RUL_connected;
            vec3.scale(vector_restore,rabbit_RUL_connected,-1);
            
            mat4.translate(
                this.RabbitRULMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitRULMesh.world, current_matrix,
                rabbit_RUL_rotation
            );
            mat4.translate(
                this.RabbitRULMesh.world, current_matrix,
                vector_restore
            );
            
            mat4.multiply(this.RabbitRRLMesh.world,base,current_matrix);
            break;

        case 'RabbitLUL':
            var speed = 0.5;
            if(me.rabbit_LUL_count>=1000){
                rabbit_LUL_direction = -1;
            }
            if (me.rabbit_LUL_count<=-1000){
                rabbit_LUL_direction = 1;
            }
            rabbit_LUL_rotation += rabbit_LUL_direction*speed*dt/1000;
            me.rabbit_LUL_count += rabbit_LUL_direction*dt;


            vector = rabbit_LUL_connected;
            vec3.scale(vector_restore,rabbit_LUL_connected,-1);
            
            mat4.translate(
                this.RabbitLULMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitLULMesh.world, current_matrix,
                rabbit_LUL_rotation
            );
            mat4.translate(
                this.RabbitLULMesh.world, current_matrix,
                vector_restore
            );

            mat4.multiply(this.RabbitLLLMesh.world,base,current_matrix);
            break;
        
        case 'RabbitRLL':
            var speed = 0.5;
            if(me.rabbit_RLL_count>=1000){
                rabbit_RLL_direction = -1;
            }
            if (me.rabbit_RLL_count<=-1000){
                rabbit_RLL_direction = 1;
            }
            rabbit_RLL_rotation += rabbit_RLL_direction*speed*dt/1000;
            me.rabbit_RLL_count += rabbit_RLL_direction*dt;


            vector = rabbit_RLL_connected;
            vec3.scale(vector_restore,rabbit_RLL_connected,-1);
            
            mat4.translate(
                this.RabbitRRLMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitRRLMesh.world, current_matrix,
                rabbit_RLL_rotation
            );
            mat4.translate(
                this.RabbitRRLMesh.world, current_matrix,
                vector_restore
            );
            
            break;

        case 'RabbitLLL':
            var speed = 0.2;
            if(me.rabbit_LLL_count>=1000){
                rabbit_LLL_direction = -1;
            }
            if (me.rabbit_LLL_count<=-1000){
                rabbit_LLL_direction = 1;
            }
            rabbit_LLL_rotation += rabbit_LLL_direction*speed*dt/1000;
            me.rabbit_LLL_count += rabbit_LLL_direction*dt;


            vector = rabbit_LLL_connected;
            vec3.scale(vector_restore,rabbit_LLL_connected,-1);
            
            mat4.translate(
                this.RabbitLLLMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitLLLMesh.world, current_matrix,
                rabbit_LLL_rotation
            );
            mat4.translate(
                this.RabbitLLLMesh.world, current_matrix,
                vector_restore
            );

            break;

        case 'RabbitTail':
            var speed = 2;
            if(me.rabbit_tail_count>=300){
                rabbit_tail_direction = -1;
            }
            if (me.rabbit_tail_count<=-300){
                rabbit_tail_direction = 1;
            }
            rabbit_tail_rotation += rabbit_tail_direction*speed*dt/1000;
            me.rabbit_tail_count += rabbit_tail_direction*dt;


            vector = rabbit_tail_connected;
            vec3.scale(vector_restore,rabbit_tail_connected,-1);
            
            mat4.translate(
                this.RabbitTailMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.RabbitTailMesh.world, current_matrix,
                rabbit_tail_rotation
            );
            mat4.translate(
                this.RabbitTailMesh.world, current_matrix,
                vector_restore
            );

            break;
        
        case 'TruckBody':
            var translation_speed = 0.02;
            if(me.truck_body_translation_count>=1.5/translation_speed){
                truck_body_translation_direction = -1;
            }
            if (me.truck_body_translation_count<=-1.5/translation_speed){
                truck_body_translation_direction = 1;
            }
            if (dt==0){
                truck_body_translation_direction = 0;
            }
            me.truck_body_translation_count += truck_body_translation_direction;
            
            mat4.multiply(this.TruckBodyMesh.world,current_matrix,basematrix);

            vector = truck_body_base;
            vec3.scale(vector_restore,truck_body_base,-1);
            
            mat4.translate(
                this.TruckBodyMesh.world, this.TruckBodyMesh.world,
                vector
            );
            mat4.translate(
                this.TruckBodyMesh.world, this.TruckBodyMesh.world,
                vec3.fromValues(0,0,truck_body_translation_direction*translation_speed)
            );
            mat4.translate(
                this.TruckBodyMesh.world, this.TruckBodyMesh.world,
                vector_restore
            );
            // Update center of body
            vec3.add(truck_body_base,vector,vec3.fromValues(0,0,truck_body_translation_direction*translation_speed));
            
            // Count distance difference
            vec3.add(me.truck_distance_diff,me.truck_distance_diff,vec3.fromValues(0,0,truck_body_translation_direction*translation_speed*-1));
            

            // Multiply children
            mat4.multiply(this.TruckHeadMesh.world,basematrix,current_matrix);
            mat4.multiply(this.TruckFLWheelMesh.world,basematrix,current_matrix);
            mat4.multiply(this.TruckFRWheelMesh.world,basematrix,current_matrix);
            mat4.multiply(this.TruckRLWheelMesh.world,basematrix,current_matrix);
            mat4.multiply(this.TruckRRWheelMesh.world,basematrix,current_matrix);
            break;

        case 'TruckHead':
            var speed = 0.5;
            if(me.truck_head_count>=500){
                truck_head_direction = -1;
            }
            if (me.truck_head_count<=-500){
                truck_head_direction = 1;
            }
            truck_head_rotation += truck_head_direction*speed*dt/1000;
            me.truck_head_count += truck_head_direction*dt;


            vector = truck_head_connected;
            vec3.scale(vector_restore,truck_head_connected,-1);
            
            mat4.translate(
                this.TruckHeadMesh.world, current_matrix,
                vector
            );
            mat4.rotateZ(
                this.TruckHeadMesh.world, current_matrix,
                truck_head_rotation
            );
            mat4.translate(
                this.TruckHeadMesh.world, current_matrix,
                vector_restore
            );

            mat4.multiply(this.TruckLaserLeftMesh.world,base,current_matrix);
            mat4.multiply(this.TruckLaserRightMesh.world,base,current_matrix);
            break;
        
        case 'TruckFLWheel':
            var speed = 1;
            if(me.truck_flwheel_count>=3750){
                truck_flwheel_direction = -1;
            }
            if (me.truck_flwheel_count<=-3750){
                truck_flwheel_direction = 1;
            }
            truck_flwheel_rotation += truck_flwheel_direction*speed*dt/1000;
            me.truck_flwheel_count += truck_flwheel_direction*dt;
            
            vector = truck_flwheel_connected;
            vec3.scale(vector_restore,truck_flwheel_connected,-1);
            
            mat4.translate(
                this.TruckFLWheelMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.TruckFLWheelMesh.world, current_matrix,
                truck_flwheel_rotation
            );
            mat4.translate(
                this.TruckFLWheelMesh.world, current_matrix,
                vector_restore
            );

            break;
        
        case 'TruckFRWheel':
            var speed = 1;
            if(me.truck_frwheel_count>=4000){
                truck_frwheel_direction = -1;
            }
            if (me.truck_frwheel_count<=1500){
                truck_frwheel_direction = 1;
            }
            truck_frwheel_rotation += truck_frwheel_direction*speed*dt/1000;
            me.truck_frwheel_count += truck_frwheel_direction*dt;
            
            vector = truck_frwheel_connected;
            vec3.scale(vector_restore,truck_frwheel_connected,-1);
            
            mat4.translate(
                this.TruckFRWheelMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.TruckFRWheelMesh.world, current_matrix,
                truck_frwheel_rotation
            );
            mat4.translate(
                this.TruckFRWheelMesh.world, current_matrix,
                vector_restore
            );

            break;
        
        case 'TruckRLWheel':
            var speed = 1;
            if(me.truck_rlwheel_count>=4000){
                truck_rlwheel_direction = -1;
            }
            if (me.truck_rlwheel_count<=-1500){
                truck_rlwheel_direction = 1;
            }
            truck_rlwheel_rotation += truck_rlwheel_direction*speed*dt/1000;
            me.truck_rlwheel_count += truck_rlwheel_direction*dt;
            
            vector = truck_rlwheel_connected;
            vec3.scale(vector_restore,truck_rlwheel_connected,-1);
            
            mat4.translate(
                this.TruckRLWheelMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.TruckRLWheelMesh.world, current_matrix,
                truck_rlwheel_rotation
            );
            mat4.translate(
                this.TruckRLWheelMesh.world, current_matrix,
                vector_restore
            );

            break;
        
        case 'TruckRRWheel':
            var speed = 1;
            if(me.truck_rrwheel_count>=4000){
                truck_rrwheel_direction = -1;
            }
            if (me.truck_rrwheel_count<=-1500){
                truck_rrwheel_direction = 1;
            }
            truck_rrwheel_rotation += truck_rrwheel_direction*speed*dt/1000;
            me.truck_rrwheel_count += truck_rrwheel_direction*dt;
            
            vector = truck_rrwheel_connected;
            vec3.scale(vector_restore,truck_rrwheel_connected,-1);
            
            mat4.translate(
                this.TruckRRWheelMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.TruckRRWheelMesh.world, current_matrix,
                truck_rrwheel_rotation
            );
            mat4.translate(
                this.TruckRRWheelMesh.world, current_matrix,
                vector_restore
            );

            break;
        
        case 'TruckLeftLaser':
            var speed = 1;
            if(me.truck_laser_left_count<=-1750/speed){
                truck_laser_left_direction = 1;
            }
            if (me.truck_laser_left_count>=0){
                truck_laser_left_direction = -1;
            }
            truck_laser_left_rotation += truck_laser_left_direction*speed*dt/1000;
            me.truck_laser_left_count += truck_laser_left_direction*dt;
            
            vector = truck_laser_left_connected;
            vec3.scale(vector_restore,truck_laser_left_connected,-1);
            
            mat4.translate(
                this.TruckLaserLeftMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.TruckLaserLeftMesh.world, current_matrix,
                truck_laser_left_rotation
            );
            mat4.translate(
                this.TruckLaserLeftMesh.world, current_matrix,
                vector_restore
            );

            break;
        
        case 'TruckRightLaser':
            var speed = 1;
            if(me.truck_laser_right_count<=-1750/speed){
                truck_laser_right_direction = 1;
            }
            if (me.truck_laser_right_count>=0){
                truck_laser_right_direction = -1;
            }
            truck_laser_right_rotation += truck_laser_right_direction*speed*dt/1000;
            me.truck_laser_right_count += truck_laser_right_direction*dt;
            
            vector = truck_laser_right_connected;
            vec3.scale(vector_restore,truck_laser_right_connected,-1);
            
            mat4.translate(
                this.TruckLaserRightMesh.world, current_matrix,
                vector
            );
            mat4.rotateX(
                this.TruckLaserRightMesh.world, current_matrix,
                truck_laser_right_rotation
            );
            mat4.translate(
                this.TruckLaserRightMesh.world, current_matrix,
                vector_restore
            );

            break;
    
    };

};

/*
Animate figures

dt: delta time
*/
MainScene.prototype.Animate = function(dt){
    // APPLY ROTATION HEREE (ANIMATION!!!)
    
    for(var i=0; i<parts.length; i++){
        this.connectParts(parts[i],dt)
    };
};