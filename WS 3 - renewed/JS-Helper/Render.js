'use strict';
/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 3: Texturing & Shadowing

Main functions to call when render loop.
*/

/*
The Update Function for every delta.
Main Animation Programs are called from here.
*/
MainScene.prototype._Update = function (dt) {
	var me = this;
	if(this.animate_on){
		// Play animation
		this.Animate(dt);
	} else {
		this.Animate(0);
	}

	//Update slider
	for(var i=0; i<me.slider_content.length; i++){
		var temp = 0;
        var temp2 = 0;
        temp = document.getElementById(me.lst_of_slider[i]).value;
        temp2 = document.getElementById(me.lst_of_content[i]);
		temp2.innerHTML = me.slider_content[i] + temp.toString();
        
        if(me.animate_on){
			switch(me.slider_content[i]){
				//================================== ORANG ===============================//
				case 'Kepala (kiri - kanan): ':
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.human_head_count/5);
					break;

				case 'Kepala (atas - bawah): ':
					//document.getElementById(me.lst_of_slider[0]).value = Math.floor(me.human_head_count/5);
					break;

				case "Badan Orang: ":
					document.getElementById(me.lst_of_slider[i]).value = me.human_body_count;
					break;

				case "Bahu Kanan: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.human_RUA_count/8);
					break;

				case "Lengan Kanan: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.human_RLA_count/8);
					break;
				
				case "Bahu Kiri: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.human_LUA_count/8);
					break;
				
				case "Lengan Kiri: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.human_LLA_count/8);
					break;

				case "Kaki Kanan Orang: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.human_right_leg_count/8);
					break;
				
				case "Kaki Kiri Orang: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.human_left_leg_count/8);
					break;
				
				//================================== KELINCI ===============================//
				case "Kuping Kiri: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_left_ear_count/10);
					break;
				
				case "Kuping Kanan: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_right_ear_count/10);
					break;

				case "Kepala Kelinci: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_head_count/10);
					break;

				case "Badan Kelinci: ":
					document.getElementById(me.lst_of_slider[i]).value = me.rabbit_body_count;
					break;

				case "Tangan Kiri: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_left_arm_count/10);
					break;

				case "Tangan Kanan: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_right_arm_count/10);
					break;
				
				case "Paha Kiri: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_LUL_count/10);
					break;
				
				case "Kaki Kiri Kelinci: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_LLL_count/10);
					break;

				case "Paha Kanan: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_RUL_count/10);
					break;
				
				case "Kaki Kanan Kelinci: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_RLL_count/10);
					break;
				
				//================================== TRUCK ===============================//
				case "Posisi Truck: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.truck_body_translation_count*100/75);
					break;

				case "Kepala Truck: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.truck_head_count/5);
					break;

				case "Roda Kiri Depan: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.truck_flwheel_count/37.5);
					break;

				case "Roda Kanan Depan: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_right_arm_count/37.5);
					break;
				
				case "Roda Kiri Belakang: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_LUL_count/37.5);
					break;
				
				case "Roda Kanan Belakang: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_LLL_count/37.5);
					break;

				case "Paha Kanan: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_RUL_count/10);
					break;
				
				case "Kaki Kanan Kelinci: ":
					document.getElementById(me.lst_of_slider[i]).value = Math.floor(me.rabbit_RLL_count/10);
					break;
			}
		} 
		// else {
        //     slider_angles[i] = temp;
        // }
	}
	
    // Move camera from key
	this.MoveCamera(dt);

    this.camera.GetViewMatrix(this.viewMatrix);
};

MainScene.prototype._GenerateShadowMap = function () {
	var gl = this.gl;

	// Set GL state status
	gl.useProgram(this.ShadowMapGenProgram);
	gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.shadowMapCube);
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.shadowMapFramebuffer);
	gl.bindRenderbuffer(gl.RENDERBUFFER, this.shadowMapRenderbuffer);

	gl.viewport(0, 0, this.textureSize, this.textureSize);
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);

	// Set per-frame uniforms
	gl.uniform2fv(
		this.ShadowMapGenProgram.uniforms.shadowClipNearFar,
		this.shadowClipNearFar
	);
	gl.uniform3fv(
		this.ShadowMapGenProgram.uniforms.pointLightPosition,
		this.lightPosition
	);
	gl.uniformMatrix4fv(
		this.ShadowMapGenProgram.uniforms.mProj,
		gl.FALSE,
		this.shadowMapProj
	);

	for (var i = 0; i < this.shadowMapCameras.length; i++) {
		// Set per light uniforms
		gl.uniformMatrix4fv(
			this.ShadowMapGenProgram.uniforms.mView,
			gl.FALSE,
			this.shadowMapCameras[i].GetViewMatrix(this.shadowMapViewMatrices[i])
		);

		// Set framebuffer destination
		gl.framebufferTexture2D(
			gl.FRAMEBUFFER,
			gl.COLOR_ATTACHMENT0,
			gl.TEXTURE_CUBE_MAP_POSITIVE_X + i,
			this.shadowMapCube,
			0
		);
		gl.framebufferRenderbuffer(
			gl.FRAMEBUFFER,
			gl.DEPTH_ATTACHMENT,
			gl.RENDERBUFFER,
			this.shadowMapRenderbuffer
		);

		gl.clearColor(0, 0, 0, 1);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		// Draw meshes
		for (var j = 0; j < this.Meshes.length; j++) {
			// Per object uniforms
			gl.uniformMatrix4fv(
				this.ShadowMapGenProgram.uniforms.mWorld,
				gl.FALSE,
				this.Meshes[j].world
			);

			// Set attributes
			if(j == 3 || j == 5 || j == 12 || j == 4 || j == 9 || j == 6 || j == 8 || j == 7 || j == 10 || j == 11){
				// Vertices Mapping for texture meshes
				gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[j].vbo);
				gl.vertexAttribPointer(
					this.ShadowProgram.attribs.vPos,
					3, gl.FLOAT, gl.FALSE,
					5 * Float32Array.BYTES_PER_ELEMENT, 0
				);
				gl.enableVertexAttribArray(this.ShadowMapGenProgram.attribs.vPos);
				
			} else{
				gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[j].vbo);
				gl.vertexAttribPointer(
					this.ShadowMapGenProgram.attribs.vPos,
					3, gl.FLOAT, gl.FALSE,
					0, 0
				);
				gl.enableVertexAttribArray(this.ShadowMapGenProgram.attribs.vPos);
			}

			gl.bindBuffer(gl.ARRAY_BUFFER, null);

			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.Meshes[j].ibo);
			gl.drawElements(gl.TRIANGLES, this.Meshes[j].nPoints, gl.UNSIGNED_SHORT, 0);
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
		}
	}

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.bindRenderbuffer(gl.RENDERBUFFER, null);
	gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
};

// The Main Render Function to be looped
MainScene.prototype._Render = function () {
    var gl = this.gl;

	// Clear back buffer, set per-frame uniforms
	gl.enable(gl.CULL_FACE);
	gl.enable(gl.DEPTH_TEST);

	gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

	gl.clearColor(0, 0, 0, 1);
	gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

	gl.useProgram(this.ShadowProgram);
	gl.uniformMatrix4fv(this.ShadowProgram.uniforms.mProj, gl.FALSE, this.projMatrix);
	gl.uniformMatrix4fv(this.ShadowProgram.uniforms.mView, gl.FALSE, this.viewMatrix);
	gl.uniform3fv(this.ShadowProgram.uniforms.pointLightPosition, this.lightPosition);

	// Shadow Vars Bind
	gl.uniform2fv(this.ShadowProgram.uniforms.shadowClipNearFar, this.shadowClipNearFar);
	gl.uniform1i(this.ShadowProgram.uniforms.lightShadowMap, 0);
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.shadowMapCube);
    
    // Draw meshes
	for (var i = 0; i < this.Meshes.length; i++) {
		// Per object uniforms
		gl.uniformMatrix4fv(
			this.ShadowProgram.uniforms.mWorld,
			gl.FALSE,
			this.Meshes[i].world
		);
		gl.uniform4fv(
			this.ShadowProgram.uniforms.meshColor,
			this.Meshes[i].color
		);

		// Add texture to selected object
		// Texture need special treatment for vertices and load textur in it
		// 3 = wall, 5 = human head, 12 = rabbit head, 4 = human body, 9 = human lla, 
		// 6 = human rua, 8 = human lua, 7 = human rla, 10 == Rleg, 11 = Lleg
		if(i == 3 || i == 12){
			this.addTexture(gl, i ,0)
		}
		else if(i == 5 || i == 4 || i == 9 || i== 6 || i == 8 || i == 7 || i == 10 || i == 11){
			this.addTexture(gl, i ,1);
		}
		else {
			// Vertices
			gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[i].vbo);
			gl.vertexAttribPointer(
				this.ShadowProgram.attribs.vPos,
				3, gl.FLOAT, gl.FALSE,
				0, 0
			);
			gl.enableVertexAttribArray(this.ShadowProgram.attribs.vPos);
			
			// Disable texture load
			gl.disableVertexAttribArray(this.ShadowProgram.attribs.vertTexCoord);
		}
		
		// Normal
		gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[i].nbo);
		gl.vertexAttribPointer(
			this.ShadowProgram.attribs.vNorm,
			3, gl.FLOAT, gl.FALSE,
			0, 0
		);
		gl.enableVertexAttribArray(this.ShadowProgram.attribs.vNorm);		

		// Cleanup
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
		// Index
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.Meshes[i].ibo);
		gl.drawElements(gl.TRIANGLES, this.Meshes[i].nPoints, gl.UNSIGNED_SHORT, 0);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
	}
};

MainScene.prototype.addTexture = function (gl, i, idx) {
	// Vertices
	gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[i].vbo);
	gl.vertexAttribPointer(
		this.ShadowProgram.attribs.vPos,
		3, gl.FLOAT, gl.FALSE,
		5 * Float32Array.BYTES_PER_ELEMENT, 0
	);
	gl.enableVertexAttribArray(this.ShadowProgram.attribs.vPos);

	// texture
	gl.vertexAttribPointer(
		this.ShadowProgram.attribs.vertTexCoord, // Attribute location
		2, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		5 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		3 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
	);
	gl.enableVertexAttribArray(this.ShadowProgram.attribs.vertTexCoord);

	if(idx == 0){
		gl.activeTexture(gl.TEXTURE1);

		// Bind Texture
		gl.bindTexture(gl.TEXTURE_2D, this.textures[0]);

		gl.uniform1i(this.ShadowProgram.uniforms.img0, 1);  // texture unit 0
	}
	else if(idx == 1){
		gl.uniform1i(this.ShadowProgram.uniforms.img1, 1);  // texture unit 1
		
		// Bind Texture
		gl.bindTexture(gl.TEXTURE_2D, this.textures[1]);
		
		gl.activeTexture(gl.TEXTURE2);
	} 
	
};
