'use strict';
/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 3: Texturing & Shadowing

Array flattening source:
http://stackoverflow.com/questions/10865025/merge-flatten-a-multidimensional-array-in-javascript

Main Scene to run the program.
*/

var MainScene = function (gl) {
	this.gl = gl;
};

/*
Function to load main program.
*/
MainScene.prototype.Load = function(cb) {
    console.log("Loading Main Scene");
	var me = this;

	me.lst_of_slider = ["s1","s2","s3","s4","s5","s6","s7","s8","s9","s10",
					"s11","s12","s13","s14","s15","s16","s17","s18","s19",
					"s20","s21","s22","s23","s24","s25","s26","s27"];
	
	me.lst_of_content = ["c1","c2","c3","c4","c5","c6","c7","c8","c9","c10",
					"c11","c12","c13","c14","c15","c16","c17","c18","c19",
					"c20","c21","c22","c23","c24","c25","c26","c27"];
	
	// Text to be displayed in order of each part id's
	me.slider_content = [
		"Kepala (kiri - kanan): ",  // 0
		"Kepala (atas - bawah): ", // 1
		"Badan Orang: ",  // 2
		"Bahu Kanan: ",  // 3
		"Lengan Kanan: ",  // 4
		"Bahu Kiri: ", // 5
		"Lengan Kiri: ",  // 6
		"Kaki Kanan Orang: ",  // 7
		"Kaki Kiri Orang: ",  // 8
	
		"Kuping Kiri: ",  // 9
		"Kuping Kanan: ", // 10
		"Kepala Kelinci: ", // 11
		"Badan Kelinci: ", // 12
		"Tangan Kiri: ", // 13
		"Tangan Kanan: ", // 14
		"Paha Kiri: ", // 15
		"Kaki Kiri Kelinci: ", // 16
		"Paha Kanan: ", // 17
		"Kaki Kanan Kelinci: ", // 18

		"Posisi Truck: ", // 19
		"Kepala Truck: ", // 20
		"Roda Kiri Depan: ", // 21
		"Roda Kanan Depan: ", // 22
		"Roda Kiri Belakang: ", // 23
		"Roda Kanan Belakang: ", // 24
		"Laser Kiri: ", // 25
		"Laser Kanan: " // 26
	];

	
	
	// Set Animation Togle
	me.animate_on = true;
	var ani = document.getElementById("ButtonAnimate");
    ani.addEventListener("click", function(){
		if(me.animate_on){
			me.animate_on = false;
			ani.innerHTML = "Restart Animation";
		}
		else{
			me.ResetFigures();
			me.animate_on = true;
			ani.innerHTML = "Stop Animation";
		}
	});
	
	// Set Lightning Togle
	me.light_on = true;
	var lamp = document.getElementById("ButtonLight");
    lamp.addEventListener("click", function(){
		if(me.light_on){
			me.light_on = false;
			lamp.innerHTML = "Lights On";
		}
		else{
			me.light_on = true;
			lamp.innerHTML = "Lights Out";
		}
		me.switchLight(me.light_on);
	});


    // Set Async
    async.parallel({
        // Open JSON
        Models: function (callback) {
            async.map({
                SceneModel: "WS3.json"
            }, LoadJSONResource, callback);
        },
        // Open Shader Text
        ShaderCode: function (callback) {
            async.map({
				'Shadow_VSText': 'shaders/Shadow.vs.glsl',
				'Shadow_FSText': 'shaders/Shadow.fs.glsl',
				'ShadowMapGen_VSText': 'shaders/ShadowMapGen.vs.glsl',
				'ShadowMapGen_FSText': 'shaders/ShadowMapGen.fs.glsl'
            }, LoadTextResource, callback);
        }
    }, function(loadErrors, results) {
        if (loadErrors) {
            cb(loadErrors);
            return;
        }

        /*
            Create Model objects
            Taken by each Mesh from Blender files.
            Assigned to attributes of me.
		*/
		for (var i = 0; i < results.Models.SceneModel.meshes.length; i++) {
			var mesh = results.Models.SceneModel.meshes[i];
			var color;
			switch (mesh.name) {
				// APPLY MATRICES: SCALE - ROTATE - TRANSLATE

				case 'TableMesh':
					color = hexToRgb("#2b5acf")
					me.TableMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.TableMesh.world, me.TableMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'SofaMesh':
					color = hexToRgb("#ffba30")
					me.SofaMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.SofaMesh.world, me.SofaMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'LightBulbMesh':
					// LightPosition for point light source
					me.lightPosition = vec3.fromValues(0.90312, 1.60117, 3.1);
					me.LightMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(4, 4, 4, 1)
					);
					mat4.rotate(
						me.LightMesh.world, me.LightMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'WallsMesh':
					color = hexToRgb("#474747")
					me.WallsMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.WallsMesh.world, me.WallsMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				//////////////// HUMAN /////////////////

				case 'HumanBodyMesh':
					color = hexToRgb("#474747")
					me.HumanBodyMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.HumanBodyMesh.world, me.HumanBodyMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'HumanHeadMesh':
					color = hexToRgb("#474747")
					me.HumanHeadMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.HumanHeadMesh.world, me.HumanHeadMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'HumanRUAMesh':
					color = hexToRgb("#474747")
					me.HumanRUAMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.HumanRUAMesh.world, me.HumanRUAMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'HumanRLAMesh':
					color = hexToRgb("#474747")
					me.HumanRLAMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.HumanRLAMesh.world, me.HumanRLAMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'HumanLUAMesh':
					color = hexToRgb("#474747")
					me.HumanLUAMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.HumanLUAMesh.world, me.HumanLUAMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'HumanLLAMesh':
					color = hexToRgb("#474747")
					me.HumanLLAMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.HumanLLAMesh.world, me.HumanLLAMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'HumanRightLegMesh':
					color = hexToRgb("#474747")
					me.HumanRightLegMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.HumanRightLegMesh.world, me.HumanRightLegMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'HumanLeftLegMesh':
					color = hexToRgb("#474747")
					me.HumanLeftLegMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.HumanLeftLegMesh.world, me.HumanLeftLegMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				////////// RABBIT ////////////

				case 'RabbitBodyMesh':
					color = hexToRgb("#474747")
					me.RabbitBodyMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitBodyMesh.world, me.RabbitBodyMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitHeadMesh':
					color = hexToRgb("#29adf0")
					me.RabbitHeadMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitHeadMesh.world, me.RabbitHeadMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitLeftEarMesh':
					color = hexToRgb("#f78843")
					me.RabbitLeftEarMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitLeftEarMesh.world, me.RabbitLeftEarMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitRightEarMesh':
					color = hexToRgb("#f78843")
					me.RabbitRightEarMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitRightEarMesh.world, me.RabbitRightEarMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitLeftArmMesh':
					color = hexToRgb("#e8eb46")
					me.RabbitLeftArmMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitLeftArmMesh.world, me.RabbitLeftArmMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitRightArmMesh':
					color = hexToRgb("#e8eb46")
					me.RabbitRightArmMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitRightArmMesh.world, me.RabbitRightArmMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitLULMesh':
					color = hexToRgb("#bc53f5")
					me.RabbitLULMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitLULMesh.world, me.RabbitLULMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitLLLMesh':
					color = hexToRgb("#2226f5")
					me.RabbitLLLMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitLLLMesh.world, me.RabbitLLLMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitRULMesh':
					color = hexToRgb("#bc53f5")
					me.RabbitRULMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitRULMesh.world, me.RabbitRULMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitRRLMesh':
					color = hexToRgb("#2226f5")
					me.RabbitRRLMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitRRLMesh.world, me.RabbitRRLMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;

				case 'RabbitTailMesh':
					color = hexToRgb("#6af24b")
					me.RabbitTailMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.RabbitTailMesh.world, me.RabbitTailMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;
				
				////////// TRUCK ////////////
				case 'TruckBodyMesh':
					color = hexToRgb("#eb4034")
					me.TruckBodyMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.TruckBodyMesh.world, me.TruckBodyMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;
				
				case 'TruckFLWheelMesh':
					color = hexToRgb("#2226f5")
					me.TruckFLWheelMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.TruckFLWheelMesh.world, me.TruckFLWheelMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;
				
				case 'TruckFRWheelMesh':
					color = hexToRgb("#2226f5")
					me.TruckFRWheelMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.TruckFRWheelMesh.world, me.TruckFRWheelMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;
				
				case 'TruckHeadMesh':
					color = hexToRgb("#29adf0")
					me.TruckHeadMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.TruckHeadMesh.world, me.TruckHeadMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;
				
				case 'TruckLaserLeftMesh':
					color = hexToRgb("#e8eb46")
					me.TruckLaserLeftMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.TruckLaserLeftMesh.world, me.TruckLaserLeftMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;
				
				case 'TruckLaserRightMesh':
					color = hexToRgb("#e8eb46")
					me.TruckLaserRightMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.TruckLaserRightMesh.world, me.TruckLaserRightMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;
				
				case 'TruckRLWheelMesh':
					color = hexToRgb("#2226f5")
					me.TruckRLWheelMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.TruckRLWheelMesh.world, me.TruckRLWheelMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;
				
				case 'TruckRRWheelMesh':
					color = hexToRgb("#2226f5")
					me.TruckRRWheelMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(color[0], color[1], color[2], color[3])
					);
					mat4.rotate(
						me.TruckRRWheelMesh.world, me.TruckRRWheelMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					break;
			}
        }

        // console.log(results);
		
		me.ValidateMeshes();
        
        // Arr of meshes
		me.Meshes = [
			me.TableMesh,
			me.SofaMesh,
			me.LightMesh,
			me.WallsMesh,
			// Human
			me.HumanBodyMesh,
			me.HumanHeadMesh,
			me.HumanRUAMesh,
			me.HumanRLAMesh,
			me.HumanLUAMesh,
			me.HumanLLAMesh,
			me.HumanRightLegMesh,
			me.HumanLeftLegMesh,
			// Rabbit
			me.RabbitBodyMesh,
			me.RabbitHeadMesh,
			me.RabbitLeftEarMesh,
			me.RabbitRightEarMesh,
			me.RabbitLeftArmMesh,
			me.RabbitRightArmMesh,
			me.RabbitLULMesh,
			me.RabbitLLLMesh,
			me.RabbitRULMesh,
			me.RabbitRRLMesh,
			me.RabbitTailMesh,
			// Truck
			me.TruckBodyMesh,
			me.TruckFLWheelMesh,
			me.TruckFRWheelMesh,
			me.TruckHeadMesh,
			me.TruckLaserLeftMesh,
			me.TruckLaserRightMesh,
			me.TruckRLWheelMesh,
			me.TruckRRWheelMesh
        ];
        
        /*
            Set up Shader Program.
            Setting Variables for shaders.
		*/

		// Shadow Program
		me.ShadowProgram = StartShaderProgram(
			me.gl, results.ShaderCode.Shadow_VSText,
			results.ShaderCode.Shadow_FSText
		);
		if (me.ShadowProgram.error) {
			cb('ShadowProgram ' + me.ShadowProgram.error); return;
		}

		// Shadow Generation Program
		me.ShadowMapGenProgram = StartShaderProgram(
			me.gl, results.ShaderCode.ShadowMapGen_VSText,
			results.ShaderCode.ShadowMapGen_FSText
		);
		if (me.ShadowMapGenProgram.error) {
			cb('ShadowMapGenProgram ' + me.ShadowMapGenProgram.error); return;
		}

		// Shadow Program Uniforms & AttribS
		me.ShadowProgram.uniforms = {
			mProj: me.gl.getUniformLocation(me.ShadowProgram, 'mProj'),
			mView: me.gl.getUniformLocation(me.ShadowProgram, 'mView'),
			mWorld: me.gl.getUniformLocation(me.ShadowProgram, 'mWorld'),

			pointLightPosition: me.gl.getUniformLocation(me.ShadowProgram, 'pointLightPosition'),
			meshColor: me.gl.getUniformLocation(me.ShadowProgram, 'meshColor'),
			lightShadowMap: me.gl.getUniformLocation(me.ShadowProgram, 'lightShadowMap'),
			shadowClipNearFar: me.gl.getUniformLocation(me.ShadowProgram, 'shadowClipNearFar'),

			img0: me.gl.getUniformLocation(me.ShadowProgram, 'img0'),
			img1: me.gl.getUniformLocation(me.ShadowProgram, 'img1'),

		};
		me.ShadowProgram.attribs = {
			vPos: me.gl.getAttribLocation(me.ShadowProgram, 'vPos'),
			vNorm: me.gl.getAttribLocation(me.ShadowProgram, 'vNorm'),
			vertTexCoord: me.gl.getAttribLocation(me.ShadowProgram, 'vertTexCoord')
		};

		// Shadow Generation Uniforms & Attribs
		me.ShadowMapGenProgram.uniforms = {
			mProj: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'mProj'),
			mView: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'mView'),
			mWorld: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'mWorld'),

			pointLightPosition: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'pointLightPosition'),
			shadowClipNearFar: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'shadowClipNearFar'),
		};
		me.ShadowMapGenProgram.attribs = {
			vPos: me.gl.getAttribLocation(me.ShadowMapGenProgram, 'vPos'),
		};


		/*
		Framebuffers and Textures for SHADOW
		*/
		me.shadowMapCube = me.gl.createTexture();
		me.gl.bindTexture(me.gl.TEXTURE_CUBE_MAP, me.shadowMapCube);
		me.gl.texParameteri(me.gl.TEXTURE_CUBE_MAP, me.gl.TEXTURE_MIN_FILTER, me.gl.LINEAR);
		me.gl.texParameteri(me.gl.TEXTURE_CUBE_MAP, me.gl.TEXTURE_MAG_FILTER, me.gl.LINEAR);
		me.gl.texParameteri(me.gl.TEXTURE_CUBE_MAP, me.gl.TEXTURE_WRAP_S, me.gl.MIRRORED_REPEAT);
		me.gl.texParameteri(me.gl.TEXTURE_CUBE_MAP, me.gl.TEXTURE_WRAP_T, me.gl.MIRRORED_REPEAT);
		for (var i = 0; i < 6; i++) {
			me.gl.texImage2D(
				me.gl.TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, me.gl.RGBA,
				me.textureSize, me.textureSize,
				0, me.gl.RGBA,
				me.gl.UNSIGNED_BYTE, null
			);
		}

		me.shadowMapFramebuffer = me.gl.createFramebuffer();
		me.gl.bindFramebuffer(me.gl.FRAMEBUFFER, me.shadowMapFramebuffer);

		me.shadowMapRenderbuffer = me.gl.createRenderbuffer();
		me.gl.bindRenderbuffer(me.gl.RENDERBUFFER, me.shadowMapRenderbuffer);
		me.gl.renderbufferStorage(
			me.gl.RENDERBUFFER, me.gl.DEPTH_COMPONENT16,
			me.textureSize, me.textureSize
		);

		me.gl.bindTexture(me.gl.TEXTURE_CUBE_MAP, null);
		me.gl.bindRenderbuffer(me.gl.RENDERBUFFER, null);
		me.gl.bindFramebuffer(me.gl.FRAMEBUFFER, null);

        /*
            Logical variables
            - Camera
            - Proj & view
        */
		me.camera = new Camera(
			vec3.fromValues(-4.5, -4.8, 1.75),
			vec3.fromValues(-0.3, 1, 1.75),
			vec3.fromValues(0, 0, 1)
		);

		me.projMatrix = mat4.create();
		me.viewMatrix = mat4.create();

		mat4.perspective(
			me.projMatrix,
			glMatrix.toRadian(90),
			me.gl.canvas.width / me.gl.canvas.height,
			0.35,
			85.0
		);

		// Shadow Map Camera
		me.shadowMapCameras = [
			// Positive X
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(1, 0, 0)),
				vec3.fromValues(0, -1, 0)
			),
			// Negative X
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(-1, 0, 0)),
				vec3.fromValues(0, -1, 0)
			),
			// Positive Y
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(0, 1, 0)),
				vec3.fromValues(0, 0, 1)
			),
			// Negative Y
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(0, -1, 0)),
				vec3.fromValues(0, 0, -1)
			),
			// Positive Z
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(0, 0, 1)),
				vec3.fromValues(0, -1, 0)
			),
			// Negative Z
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(0, 0, -1)),
				vec3.fromValues(0, -1, 0)
			),
		];
		me.shadowMapViewMatrices = [
			mat4.create(),
			mat4.create(),
			mat4.create(),
			mat4.create(),
			mat4.create(),
			mat4.create()
		];
		me.shadowMapProj = mat4.create();
		me.shadowClipNearFar = vec2.fromValues(0.05, 15.0);
		mat4.perspective(
			me.shadowMapProj,
			glMatrix.toRadian(90),
			1.0,
			me.shadowClipNearFar[0],
			me.shadowClipNearFar[1]
		);

		cb();
	});
	
	// Keys For Camera Handler
	me.PressedKeys = {
		Right: false,
		Left: false,
		Forward: false,
		Back: false,

		Up: false,
		Down: false,
		RotLeft: false,
		RotRight: false,
	};

	me.MoveForwardSpeed = 3.5;
	me.RotateSpeed = 1.5;
	me.textureSize = getParameterByName('texSize') || 512;
};

/*
Function to unload main program.
After scene is ended.
Unset all GPU Resources.
*/
MainScene.prototype.Unload = function() {
    // Meshes
    this.LightMesh = null;
	this.TableMesh = null;
	this.SofaMesh = null;
	this.WallsMesh = null;

	//// Human Mesh
	this.HumanBodyMesh = null;
	this.HumanHeadMesh = null;
	this.HumanRUAMesh = null;
	this.HumanRLAMesh = null;
	this.HumanLUAMesh = null;
	this.HumanLLAMesh = null;
	this.HumanRightLegMesh = null;
	this.HumanLeftLegMesh = null;
	
	//// Rabit Mesh
	this.RabbitBodyMesh = null;
	this.RabbitHeadMesh = null;
	this.RabbitLeftEarMesh = null;
	this.RabbitRightEarMesh = null;
	this.RabbitLeftArmMesh = null;
	this.RabbitRightArmMesh = null;
	this.RabbitLULMesh = null;
	this.RabbitLLLMesh = null;
	this.RabbitRULMesh = null;
	this.RabbitRRLMesh = null;
	this.RabbitTailMesh = null;

	//// Truck Mesh
	this.TruckBodyMesh = null;
	this.TruckFLWheelMesh = null;
	this.TruckFRWheelMesh = null;
	this.TruckHeadMesh = null;
	this.TruckLaserLeftMesh = null;
	this.TruckLaserRightMesh = null;
	this.TruckRLWheelMesh = null;
	this.TruckRRWheelMesh = null;

    this.Meshes = null;

    // Program
	this.ShadowProgram = null;
	this.ShadowMapGenProgram = null;

    // Camera & Light Pos
	this.camera = null;
	this.lightPosition = null;

	// Pressed Keys
	this.PressedKeys = null;
	this.MoveForwardSpeed = null;
	this.RotateSpeed = null;

	// Shadow
	this.shadowMapCube = null;
	this.textureSize = null;

	this.shadowMapCameras = null;
	this.shadowMapViewMatrices = null;
};

/*
Function to begin the program.
Includes rendering process.
*/
MainScene.prototype.Begin = function() {
    console.log("Beginning Main Scene");

	var me = this;
	
	// Attach event listeners
	this.__WindowResizeListener = this._OnWindowResize.bind(this);
	this.__KeyDownWindowListener = this._OnKeyDown.bind(this);
	this.__KeyUpWindowListener = this._OnKeyUp.bind(this);

	AddEvent(window, 'resize', this.__WindowResizeListener);
	AddEvent(window, 'keydown', this.__KeyDownWindowListener);
	AddEvent(window, 'keyup', this.__KeyUpWindowListener);

	// Create Texture(s)
	var gl = me.gl;

	this.textures = [];
	var images = ['env-image','human-image'];
	var size = [1024, 256];
	for (var ii = 0; ii < images.length; ++ii) {
		var newTexture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, newTexture);
	
		// Set the parameters so we can render any size image.
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	
		// Upload the image into the texture.
		gl.texImage2D(
			me.gl.TEXTURE_2D,
			0, me.gl.RGBA,
			me.gl.RGBA,
			me.gl.UNSIGNED_BYTE,
			document.getElementById(images[ii])
		);
	
		// add the texture to the array of textures.
		this.textures.push(newTexture);
	}
	gl.bindTexture(gl.TEXTURE_2D, null);

	// Generate figures
	me.InitFigureMatrices();
	me.InitFigureVars();
	me.ResetFigures();

    // Render Loop
	var previousFrame = performance.now();
	var dt = 0;
	var loop = function (currentFrameTime) {
		dt = currentFrameTime - previousFrame;
		me._Update(dt);
		previousFrame = currentFrameTime;

		me._GenerateShadowMap();

		me._Render();
		me.nextFrameHandle = requestAnimationFrame(loop);
	};
	me.nextFrameHandle = requestAnimationFrame(loop);
	
	me._OnWindowResize();
};

/*
Function to end the program.
After scene is done.
*/
MainScene.prototype.End = function() {
	// Remove Event Listeners
	if (this.__WindowResizeListener) {
		RemoveEvent(window, 'resize', this.__WindowResizeListener);
	}
	if (this.__KeyUpWindowListener) {
		RemoveEvent(window, 'keyup', this.__KeyUpWindowListener);
	}
	if (this.__KeyDownWindowListener) {
		RemoveEvent(window, 'keydown', this.__KeyDownWindowListener);
	}

	// Stop Render
    if (this.nextFrameHandle) {
		cancelAnimationFrame(this.nextFrameHandle);
	}
};

/*
Function to validate creation of meshes
*/
MainScene.prototype.ValidateMeshes = function() {
	var me = this;
	// Validate Meshes
	if (!me.TableMesh) {
		cb('Failed to load table mesh'); return;
	}
	if (!me.SofaMesh) {
		cb('Failed to load sofa mesh'); return;
	}
	if (!me.LightMesh) {
		cb('Failed to load light mesh'); return;
	}
	if (!me.WallsMesh) {
		cb('Failed to load walls mesh'); return;
	}

	// HUMAN
	if (!me.HumanBodyMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.HumanHeadMesh) {
		cb('Failed to load human head mesh'); return;
	}
	if (!me.HumanRUAMesh) {
		cb('Failed to load human rua mesh'); return;
	}
	if (!me.HumanRLAMesh) {
		cb('Failed to load human rla mesh'); return;
	}
	if (!me.HumanLUAMesh) {
		cb('Failed to load human lua mesh'); return;
	}
	if (!me.HumanLLAMesh) {
		cb('Failed to load human lla mesh'); return;
	}
	if (!me.HumanRightLegMesh) {
		cb('Failed to load human rl mesh'); return;
	}
	if (!me.HumanLeftLegMesh) {
		cb('Failed to load human ll mesh'); return;
	}
	
	// RABBIT
	if (!me.RabbitBodyMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitHeadMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitLeftEarMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitRightEarMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitLeftArmMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitRightArmMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitLULMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitLLLMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitRULMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitRRLMesh) {
		cb('Failed to load human body mesh'); return;
	}
	if (!me.RabbitTailMesh) {
		cb('Failed to load human body mesh'); return;
	}

	// TRUCK
	if (!me.TruckBodyMesh){
		console.error('Failed to load truck body mesh'); return;
	}
	if (!me.TruckFLWheelMesh){
		console.error('Failed to load truck front left wheel mesh'); return;
	}
	if (!me.TruckFRWheelMesh){
		console.error('Failed to load truck front right wheel mesh'); return;
	}
	if (!me.TruckFRWheelMesh){
		console.error('Failed to load truck front right wheel mesh'); return;
	}
	if (!me.TruckHeadMesh){
		console.error('Failed to load truck head mesh'); return;
	}
	if (!me.TruckLaserLeftMesh){
		console.error('Failed to load truck left laser mesh'); return;
	}
	if (!me.TruckLaserRightMesh){
		console.error('Failed to load truck right laser mesh'); return;
	}
	if (!me.TruckRLWheelMesh){
		console.error('Failed to load truck rear left wheel mesh'); return;
	}
	if (!me.TruckRRWheelMesh){
		console.error('Failed to load truck rear right wheel mesh'); return;
	}
};

MainScene.prototype.switchLight = function (light_on){
	var on = [
		hexToRgb('#2b5acf'),
		hexToRgb('#ffba30'),
		hexToRgb('#474747'),
		hexToRgb('#474747'),
		hexToRgb('#474747'),
		hexToRgb('#474747'),
		hexToRgb('#474747'),
		hexToRgb('#474747'),
		hexToRgb('#474747'),
		hexToRgb('#474747'),
		hexToRgb('#474747'),

		hexToRgb('#474747'),
		hexToRgb('#29adf0'),
		hexToRgb('#f78843'),
		hexToRgb('#f78843'),
		hexToRgb('#e8eb46'),
		hexToRgb('#e8eb46'),
		hexToRgb('#bc53f5'),
		hexToRgb('#2226f5'),
		hexToRgb('#bc53f5'),
		hexToRgb('#2226f5'),
		hexToRgb('#6af24b')
	];

	var off = [
		hexToRgb('#13295d'),
		hexToRgb('#6b4700'),
		hexToRgb('#000000'),
		hexToRgb('#000000'),
		hexToRgb('#000000'),
		hexToRgb('#000000'),
		hexToRgb('#000000'),
		hexToRgb('#000000'),
		hexToRgb('#000000'),
		hexToRgb('#000000'),
		hexToRgb('#000000'),

		hexToRgb('#000000'),
		hexToRgb('#074362'),
		hexToRgb('#5e2704'),
		hexToRgb('#5e2704'),
		hexToRgb('#61620b'),
		hexToRgb('#61620b'),
		hexToRgb('#47066a'),
		hexToRgb('#040662'),
		hexToRgb('#47066a'),
		hexToRgb('#040662'),
		hexToRgb('#134c05')

	];

	var color;
	if(light_on){
		this.TableMesh.color = vec4.fromValues(on[0][0], on[0][1], on[0][2], on[0][3]);
		this.SofaMesh.color = vec4.fromValues(on[1][0], on[1][1], on[1][2], on[1][3]);
		this.WallsMesh.color = vec4.fromValues(on[2][0], on[2][1], on[2][2], on[2][3]);
		this.HumanBodyMesh.color = vec4.fromValues(on[3][0], on[3][1], on[3][2], on[3][3]);
		this.HumanHeadMesh.color = vec4.fromValues(on[4][0], on[4][1], on[4][2], on[4][3]);
		this.HumanRUAMesh.color = vec4.fromValues(on[5][0], on[5][1], on[5][2], on[5][3]);
		this.HumanRLAMesh.color = vec4.fromValues(on[6][0], on[6][1], on[6][2], on[6][3]);
		this.HumanLUAMesh.color = vec4.fromValues(on[7][0], on[7][1], on[7][2], on[7][3]);
		this.HumanLLAMesh.color = vec4.fromValues(on[8][0], on[8][1], on[8][2], on[8][3]);
		this.HumanRightLegMesh.color = vec4.fromValues(on[9][0], on[9][1], on[9][2], on[9][3]);
		this.HumanLeftLegMesh.color = vec4.fromValues(on[10][0], on[10][1], on[10][2], on[10][3]);
		
		this.RabbitBodyMesh.color = vec4.fromValues(on[11][0], on[11][1], on[11][2], on[11][3]);
		this.RabbitHeadMesh.color = vec4.fromValues(on[12][0], on[12][1], on[12][2], on[12][3]);
		this.RabbitLeftEarMesh.color = vec4.fromValues(on[13][0], on[13][1], on[13][2], on[13][3]); 
		this.RabbitRightEarMesh.color = vec4.fromValues(on[14][0], on[14][1], on[14][2], on[14][3]); 
		this.RabbitLeftArmMesh.color = vec4.fromValues(on[15][0], on[15][1], on[15][2], on[15][3]);
		this.RabbitRightArmMesh.color = vec4.fromValues(on[16][0], on[16][1], on[16][2], on[16][3]); 
		this.RabbitLULMesh.color = vec4.fromValues(on[17][0], on[17][1], on[17][2], on[17][3]);
		this.RabbitLLLMesh.color = vec4.fromValues(on[18][0], on[18][1], on[18][2], on[18][3]);
		this.RabbitRULMesh.color = vec4.fromValues(on[19][0], on[19][1], on[19][2], on[19][3]);
		this.RabbitRRLMesh.color = vec4.fromValues(on[20][0], on[20][1], on[20][2], on[20][3]);
		this.RabbitTailMesh.color = vec4.fromValues(on[21][0], on[21][1], on[21][2], on[21][3]); 
	} 
	
	else{
		this.TableMesh.color = vec4.fromValues(off[0][0], off[0][1], off[0][2], off[0][3]);
		this.SofaMesh.color = vec4.fromValues(off[1][0], off[1][1], off[1][2], off[1][3]);
		this.WallsMesh.color = vec4.fromValues(off[2][0], off[2][1], off[2][2], off[2][3]);
		this.HumanBodyMesh.color = vec4.fromValues(off[3][0], off[3][1], off[3][2], off[3][3]);
		this.HumanHeadMesh.color = vec4.fromValues(off[4][0], off[4][1], off[4][2], off[4][3]);
		this.HumanRUAMesh.color = vec4.fromValues(off[5][0], off[5][1], off[5][2], off[5][3]);
		this.HumanRLAMesh.color = vec4.fromValues(off[6][0], off[6][1], off[6][2], off[6][3]);
		this.HumanLUAMesh.color = vec4.fromValues(off[7][0], off[7][1], off[7][2], off[7][3]);
		this.HumanLLAMesh.color = vec4.fromValues(off[8][0], off[8][1], off[8][2], off[8][3]);
		this.HumanRightLegMesh.color = vec4.fromValues(off[9][0], off[9][1], off[9][2], off[9][3]);
		this.HumanLeftLegMesh.color = vec4.fromValues(off[10][0], off[10][1], off[10][2], off[10][3]);
		
		this.RabbitBodyMesh.color = vec4.fromValues(off[11][0], off[11][1], off[11][2], off[11][3]);
		this.RabbitHeadMesh.color = vec4.fromValues(off[12][0], off[12][1], off[12][2], off[12][3]);
		this.RabbitLeftEarMesh.color = vec4.fromValues(off[13][0], off[13][1], off[13][2], off[13][3]); 
		this.RabbitRightEarMesh.color = vec4.fromValues(off[14][0], off[14][1], off[14][2], off[14][3]); 
		this.RabbitLeftArmMesh.color = vec4.fromValues(off[15][0], off[15][1], off[15][2], off[15][3]);
		this.RabbitRightArmMesh.color = vec4.fromValues(off[16][0], off[16][1], off[16][2], off[16][3]); 
		this.RabbitLULMesh.color = vec4.fromValues(off[17][0], off[17][1], off[17][2], off[17][3]);
		this.RabbitLLLMesh.color = vec4.fromValues(off[18][0], off[18][1], off[18][2], off[18][3]);
		this.RabbitRULMesh.color = vec4.fromValues(off[19][0], off[19][1], off[19][2], off[19][3]);
		this.RabbitRRLMesh.color = vec4.fromValues(off[20][0], off[20][1], off[20][2], off[20][3]);
		this.RabbitTailMesh.color = vec4.fromValues(off[21][0], off[21][1], off[21][2], off[21][3]); 
	} 	
};
