'use strict';
/*
Created and Developed By Kelompok Anum: 
- Raihansyah Attallah Andrian - 1706040196
- Millenio Ramadizsa - 1706040063
- Rafif Taris - 1706979436
- Gusti Ngurah Yama - 1706979253

Computer Graphics Course
Worksheet 3: Texturing & Shadowing

Array flattening source:
http://stackoverflow.com/questions/10865025/merge-flatten-a-multidimensional-array-in-javascript

The Initialize app program.
*/

var scene;

function Init() {
	var canvas = document.getElementById('gl-surface');
	var gl = canvas.getContext('webgl');
	if (!gl) {
		console.log('Failed to get WebGL context - trying experimental context');
		gl = canvas.getContext('experimental-webgl');
	}
	if (!gl) {
		alert('Your browser does not support WebGL - please use a different browser\nGoogleChrome works great!');
		return;
	}

	scene = new MainScene(gl);
	scene.Load(function (sceneError) {
		if (sceneError) {
			alert('Could not load the demo - see console for more details');
			console.error(sceneError);
		} else {
			scene.Begin();
		}
	});
}