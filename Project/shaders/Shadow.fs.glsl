precision mediump float;

uniform vec3 pointLightPosition;
uniform vec4 meshColor;

uniform samplerCube lightShadowMap;
uniform vec2 shadowClipNearFar;

uniform sampler2D img0;
uniform sampler2D img1;

varying vec3 fPos;
varying vec3 fNorm;
varying vec2 fragTexCoord;

void main()
{
	vec3 toLightNormal = normalize(pointLightPosition - fPos);

	float fromLightToFrag =
		(length(fPos - pointLightPosition) - shadowClipNearFar.x)
		/
		(shadowClipNearFar.y - shadowClipNearFar.x);

	float shadowMapValue = textureCube(lightShadowMap, -toLightNormal).r;

	// Generate Texel
	highp vec4 texelColor0 = texture2D(img0, fragTexCoord);
	highp vec4 texelColor1 = texture2D(img1, fragTexCoord);

	highp vec4 texelColor = texelColor0 * texelColor1;

	// Point Lighting
	float lightIntensity = 0.6;
	if ((shadowMapValue + 0.003) >= fromLightToFrag) {
		lightIntensity += 0.4 * max(dot(fNorm, toLightNormal), 0.0);
	}

	// Pass Texel + Real Color (Directional Lighting) + Point Lighting
 	gl_FragColor = vec4((texelColor.rgb + meshColor.rgb) * lightIntensity, texelColor.a + meshColor.a);
}